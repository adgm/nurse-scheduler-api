#include "Algorithm.h"
#include "MapGenerator.h"
#include "Scheduler.h"


Algorithm::Algorithm() {
	int nurseID = 0;
	for (; nurseID < 11; nurseID++) {
		nurses.push_back(std::make_shared<Nurse>(nurseID, 36, true));
	}
	nurses.push_back(std::make_shared<Nurse>(nurseID++, 36, false));

	nurses.push_back(std::make_shared<Nurse>(nurseID++, 32, true));
	for (; nurseID < 16; nurseID++) {
		nurses.push_back(std::make_shared<Nurse>(nurseID, 20, true));
	}
}

void Algorithm::printCurrentSchedule() {
	std::cout << "End schedule: " << std::endl;
	int count = 0;
	for (auto &nurse : nurses) {
		std::cout << nurse->getShifts().toString() << std::endl;
	}
}

void Algorithm::execute() {
	static bool firstTime = true;
	MapGenerator::resetTemporaries();
	if (firstTime) {
		patternManager.generatePatterns();
		patternManager.generateNightSets(0);
	}
	bool succesfulMap = MapGenerator::generateCurrentMappingFromFile("testSchedule.txt");
	MapGenerator::printCurrentMapping();
	Scheduler::generateNextWeek(succesfulMap, 1000000, false);
	MapGenerator::printCurrentSchedule();
	bool found = false;
	while (!found) {
		while (MapGenerator::schedule[0].getCombinationSize() < WEEK_LENGTH * 5) {
			succesfulMap = MapGenerator::generateCurrentScheduleMapping();
			MapGenerator::printCurrentMapping();
			Scheduler::generateNextWeek(succesfulMap, 1000000, false);
			MapGenerator::printCurrentSchedule();
		}
		MapGenerator::trimScheduleToLast4Weeks();
		succesfulMap = MapGenerator::generateCurrentScheduleMapping();
		MapGenerator::printCurrentMapping();
		Scheduler::generateNextWeek(succesfulMap, 1000000, true);
		if (MapGenerator::schedule[0].getCombinationSize() < WEEK_LENGTH * 5) {
			MapGenerator::getLastScheduleCopy();
			Scheduler::generateNextWeek(false, 10000000, false);
			MapGenerator::printCurrentSchedule();
		}else{
			MapGenerator::printCurrentSchedule();
			MapGenerator::generateExtendedSchedules();
			std::cout << std::endl << std::endl;
			PatternManager::printExtendedPatterns(MapGenerator::lastScheduleCopy);
			std::cout << std::endl << std::endl;
			PatternManager::printExtendedPatterns(MapGenerator::schedule);
			firstTime = false;
			found = true;
		}
	}
}
