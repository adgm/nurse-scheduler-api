#include "MapGenerator.h"
#include "ScheduleChecker.h"
#include <fstream>
#include <string>
#include "PatternManager.h"
#include "Scheduler.h"

std::vector<std::vector<std::vector<ShiftCombination>>> MapGenerator::wholeDayScheduleMapping;
std::vector<std::vector<std::vector<ShiftCombination>>> MapGenerator::wholeNightScheduleMapping;
std::vector<std::vector<ShiftCombination>> MapGenerator::currentDayScheduleMapping;
std::vector<std::vector<ShiftCombination>> MapGenerator::currentNightScheduleMapping;
std::vector<ShiftCombination> MapGenerator::schedule;
std::vector<ShiftCombination> MapGenerator::lastScheduleCopy;
std::vector<std::vector<std::vector<ShiftCombination::ExtendedShiftType>>> MapGenerator::currentDivisionMapping;

bool MapGenerator::generateCurrentScheduleMapping() {

	currentDayScheduleMapping.clear();
	currentDayScheduleMapping.resize(16);

	currentNightScheduleMapping.clear();
	currentNightScheduleMapping.resize(16);

	std::shared_ptr<Nurse> lowHourNurse = std::make_shared<Nurse>(0, 20, true);
	std::shared_ptr<Nurse> midHourNurse = std::make_shared<Nurse>(1, 32, true);
	std::shared_ptr<Nurse> highHourNurse = std::make_shared<Nurse>(2, 36, true);
	std::shared_ptr<Nurse> highHourNurseNoNights = std::make_shared<Nurse>(3, 36, false);

	for (int i = 0; i < 11; i++) {
		addToScheduleMappingIfEligible(i, PatternManager::noCostNightPatternsHighHours,
			PatternManager::lowCostNightPatternsHighHours, highHourNurse, currentNightScheduleMapping);
		addToScheduleMappingIfEligible(i, PatternManager::noCostDayPatternsHighHours,
		    PatternManager::lowCostDayPatternsHighHours, highHourNurse, currentDayScheduleMapping);
	}

	addToScheduleMappingIfEligible(11, PatternManager::noCostDayPatternsHighHours,
		PatternManager::lowCostDayPatternsHighHours, highHourNurseNoNights, currentDayScheduleMapping);

	addToScheduleMappingIfEligible(12, PatternManager::noCostDayPatternsHighHours,
		PatternManager::lowCostDayPatternsHighHours, midHourNurse, currentDayScheduleMapping);
	addToScheduleMappingIfEligible(12, PatternManager::noCostNightPatternsHighHours,
		PatternManager::lowCostNightPatternsHighHours, midHourNurse, currentNightScheduleMapping);

	for (int i = 13; i < 16; i++) {
		addToScheduleMappingIfEligible(i, PatternManager::noCostNightPatternsLowHours,
			PatternManager::lowCostNightPatternsLowHours, lowHourNurse, currentNightScheduleMapping);
		addToScheduleMappingIfEligible(i, PatternManager::noCostDayPatternsLowHours,
			PatternManager::lowCostDayPatternsLowHours, lowHourNurse, currentDayScheduleMapping);
	}

	wholeNightScheduleMapping.push_back(currentNightScheduleMapping);
	wholeDayScheduleMapping.push_back(currentDayScheduleMapping);

	for (int i = 0; i < currentDayScheduleMapping.size(); i++) {
		if (currentDayScheduleMapping[i].size() == 0) {
			return false;
		}
	}
	return true;
}

void MapGenerator::generateCurrentDivisionMapping(std::vector<ShiftCombination> &combinations, int currentDay) {
	if(currentDivisionMapping.size() != NURSE_NUMBER){
		currentDivisionMapping.clear();
		currentDivisionMapping.resize(NURSE_NUMBER);
	}
	
	int currentDivisionCost = 0;
	int totalDivisionCost = 0;
	for (int i = 0; i < NURSE_NUMBER; i++) {
		if(currentDivisionMapping[i].size() != 35){
			currentDivisionMapping[i].resize(35);
		}
		if (combinations[i][currentDay] == ShiftCombination::ShiftType::DAY) {
			if (combinations[i].getExtendedShift(currentDay - 1) != ShiftCombination::ExtendedShiftType::LATE) {
				ShiftCombination tempCombination = combinations[i];
				currentDivisionCost = SoftPartialConstraints::calculateDivisionCost(tempCombination, currentDay - 1);
				tempCombination.appendExtendedShiftOnDay(ShiftCombination::ExtendedShiftType::EARLY);
				if (SoftPartialConstraints::calculateDivisionCost(tempCombination, currentDay) == currentDivisionCost) {
					currentDivisionMapping[i][currentDay].push_back(ShiftCombination::ExtendedShiftType::EARLY);
				}
				tempCombination.changeExtendedShiftOnDay(currentDay, ShiftCombination::ExtendedShiftType::MID);
				if (SoftPartialConstraints::calculateDivisionCost(tempCombination, currentDay) == currentDivisionCost) {
					currentDivisionMapping[i][currentDay].push_back(ShiftCombination::ExtendedShiftType::MID);
				}
			}
			currentDivisionMapping[i][currentDay].push_back(ShiftCombination::ExtendedShiftType::LATE);
		}
	}
}

void MapGenerator::generateCurrentDivisionMappingForNurse(std::vector<ShiftCombination> &combinations,
	int currentDay, int nurse) {
	currentDivisionMapping[nurse][currentDay].clear();
	if (combinations[nurse][currentDay] == ShiftCombination::ShiftType::DAY) {
		if (combinations[nurse].getExtendedShift(currentDay - 1) == ShiftCombination::ExtendedShiftType::LATE) {
			currentDivisionMapping[nurse][currentDay].push_back(ShiftCombination::ExtendedShiftType::LATE);
		}
		else {
			currentDivisionMapping[nurse][currentDay].push_back(ShiftCombination::ExtendedShiftType::EARLY);
			currentDivisionMapping[nurse][currentDay].push_back(ShiftCombination::ExtendedShiftType::MID);
			currentDivisionMapping[nurse][currentDay].push_back(ShiftCombination::ExtendedShiftType::LATE);
		}
	}
}

void MapGenerator::generateExtendedSchedules(){
	generateExtendedSchedule(lastScheduleCopy);
	for(int i = 0; i < lastScheduleCopy.size(); i++){
		lastScheduleCopy[i].copyLast4WeeksExtendedCombinations(schedule[i]);
	}
	generateExtendedSchedule(schedule);
}

void MapGenerator::updateNeededLateShifts(std::vector<ShiftCombination> &schedule, int day,
	std::array<int, 3> &weekDemand, std::array<int, 3> &weekendDemand, 
	std::vector<ShiftCombination::ExtendedShiftType>& appropriateShifts){
	for (int nurse = 0; nurse < schedule.size(); nurse++) {
		if(schedule[nurse].getExtendedShift(day - 1) == ShiftCombination::ExtendedShiftType::LATE
			&& schedule[nurse][day] == ShiftCombination::ShiftType::DAY){
			appropriateShifts[nurse] = ShiftCombination::ExtendedShiftType::LATE;
			if(day % 7 < 5){
				reduceDemand(weekDemand, ShiftCombination::ExtendedShiftType::LATE);
			}else{
				reduceDemand(weekendDemand, ShiftCombination::ExtendedShiftType::LATE);
			}
		}
	}
}

void MapGenerator::generateExtendedSchedule(std::vector<ShiftCombination> &schedule){
	int offset = schedule[0].getExtendedCombinationSize();
	bool found = false;
	for(int day = offset; day < schedule[0].getCombinationSize(); day++){
		std::array<int, 3> weekDemand = { 3,3,3 };
		std::array<int, 3> weekendDemand = { 2,2,2 };
		std::vector<ShiftCombination::ExtendedShiftType> appropriateShifts;
		appropriateShifts.resize(16);
		generateCurrentDivisionMapping(schedule, day);
		updateNeededLateShifts(schedule, day, weekDemand, weekendDemand, appropriateShifts);

		for(int nurse = 0; nurse < schedule.size(); nurse++){
			if (schedule[nurse][day] == ShiftCombination::ShiftType::DAY
				&& appropriateShifts[nurse] == ShiftCombination::ExtendedShiftType::RESTING){
				found = false;
				for (int i = 0; i < currentDivisionMapping[nurse][day].size(); i++) {
					if (day % 7 < 5) {
						if (meetsDemand(weekDemand, currentDivisionMapping[nurse][day][i])){
							found = true;
							reduceDemand(weekDemand, currentDivisionMapping[nurse][day][i]);
							appropriateShifts[nurse] = currentDivisionMapping[nurse][day][i];
							break;
						}
					}else{
						if (meetsDemand(weekendDemand, currentDivisionMapping[nurse][day][i])) {
							found = true;
							reduceDemand(weekendDemand, currentDivisionMapping[nurse][day][i]);
							appropriateShifts[nurse] = currentDivisionMapping[nurse][day][i];
							break;
						}
					}
				}
				if(found == false){
//					std::cout << ",";
					generateCurrentDivisionMappingForNurse(schedule, day, nurse);
					nurse--;
				}
			}
			else {
				if (schedule[nurse][day] == ShiftCombination::ShiftType::NIGHT) {
					appropriateShifts[nurse] = ShiftCombination::ExtendedShiftType::NIGHTS;
				}
				if (schedule[nurse][day] == ShiftCombination::ShiftType::REST) {
					appropriateShifts[nurse] = ShiftCombination::ExtendedShiftType::RESTING;
				}
			}
		}
//		std::cout << "Demands left: ";
//		for(int &i : weekDemand){
//			std::cout << i << " ";
//		}
//		for (int &i : weekendDemand) {
//			std::cout << i << " ";
//		}
		updateCurrentExtendedSchedule(schedule, appropriateShifts);
		
	}
}

void MapGenerator::updateCurrentExtendedSchedule(std::vector<ShiftCombination> &schedule,
	std::vector<ShiftCombination::ExtendedShiftType> &extendedShifts){
	for(int i = 0; i < extendedShifts.size(); i++){
		schedule[i].appendExtendedShiftOnDay(extendedShifts[i]);
	}
}

bool MapGenerator::meetsDemand(std::array<int, 3> &demand, ShiftCombination::ExtendedShiftType shift){
	if(shift == ShiftCombination::ExtendedShiftType::EARLY && demand[0] < 1
		|| shift == ShiftCombination::ExtendedShiftType::MID && demand[1] < 1
		|| shift == ShiftCombination::ExtendedShiftType::LATE && demand[2] < 1){
		return false;
	}
	return true;
}

void MapGenerator::reduceDemand(std::array<int, 3> &demand, ShiftCombination::ExtendedShiftType shift) {
	if (shift == ShiftCombination::ExtendedShiftType::EARLY){
		demand[0]--;
	}
	if (shift == ShiftCombination::ExtendedShiftType::MID) {
		demand[1]--;
	}
	if (shift == ShiftCombination::ExtendedShiftType::LATE) {
		demand[2]--;
	}
}

void MapGenerator::addToScheduleMappingIfEligible(int nurseNumber, std::vector<ShiftCombination> &patterns,
	std::vector<ShiftCombination> &costPatterns, std::shared_ptr<Nurse> nurse, std::vector<std::vector<ShiftCombination>> &scheduleMapping) {
	ShiftCombination tempPattern = schedule[nurseNumber];
	ShiftCombination appendedPattern;
	int currentCost = ScheduleChecker::getCostOfInitialSchedule(nurse, tempPattern);
	if (currentCost == -1) {
		std::cout << "WRONG PATTERN INPUT!!!@#@!#!" << std::endl;
		system("pause");
		exit(-1);
	}

	for (int j = 0; j < patterns.size(); j++) {
		appendedPattern = tempPattern;
		appendedPattern.appendCombination(patterns[j]);
		if (ScheduleChecker::getCostOfPartialSchedule(nurse, appendedPattern) == currentCost) {
			scheduleMapping[nurseNumber].push_back(patterns[j]);
		}
	}

		for (int j = 0; j < costPatterns.size(); j++) {
			appendedPattern = tempPattern;
			int shiftsSize = costPatterns[j].getWorkingShiftsCount();
			appendedPattern.appendCombination(costPatterns[j]);
			int patternCost = ScheduleChecker::getCostOfPartialSchedule(nurse, appendedPattern);
			if (patternCost <= currentCost + LOW_COST && patternCost > -1) {
				scheduleMapping[nurseNumber].push_back(costPatterns[j]);
			}
		}
}

void MapGenerator::removeLastWeekFromSchedule(){
	wholeDayScheduleMapping.pop_back();
	currentDayScheduleMapping = wholeDayScheduleMapping[wholeDayScheduleMapping.size() - 1];
	wholeNightScheduleMapping.pop_back();
	currentNightScheduleMapping = wholeNightScheduleMapping[wholeNightScheduleMapping.size() - 1];
	for(int i = 0; i < schedule.size(); i++){
		schedule[i].removeLastWeekShifts();
	}
}

void MapGenerator::getLastScheduleCopy() {
	wholeDayScheduleMapping.pop_back();
	currentDayScheduleMapping = wholeDayScheduleMapping[wholeDayScheduleMapping.size() - 1];
	wholeNightScheduleMapping.pop_back();
	currentNightScheduleMapping = wholeNightScheduleMapping[wholeNightScheduleMapping.size() - 1];
	for (int i = 0; i < schedule.size(); i++) {
		schedule[i] = lastScheduleCopy[i];
	}
}

void MapGenerator::resetTemporaries(){
	wholeDayScheduleMapping.clear();
	wholeNightScheduleMapping.clear();
	currentDayScheduleMapping.clear();
	currentNightScheduleMapping.clear();
	wholeDayScheduleMapping.clear();
	wholeNightScheduleMapping.clear();
	lastScheduleCopy.clear();
	schedule.clear();
	Scheduler::resetTemporaries();
}

void MapGenerator::updateCurrentSchedule(std::vector<ShiftCombination> daySet,
	std::vector<std::pair<int, ShiftCombination>> nights) {
	int currentDayPattern = 0;
	for (int i = 0; i < schedule.size(); i++) {
		bool isNight = false;
		for (int j = 0; j < nights.size(); j++) {
			if (nights[j].first == i) {
				schedule[i].appendCombination(nights[j].second);
				isNight = true;
			}
		}
		if (!isNight) {
			schedule[i].appendCombination(daySet[currentDayPattern++]);
		}
	}
}

std::vector<ShiftCombination> MapGenerator::readScheduleFromFile(std::string filename) {
	std::ifstream infile(filename);
	if (!infile.is_open()) {
		std::cout << "Nie znaleziono pliku z 1 tygodniem!" << std::endl;
		system("pause");
		exit(-1);
	}
	std::string line;
	ShiftCombination tmp;
	while (getline(infile, line))
	{
		if(tmp.getCombinationSize() < line.size()){
			tmp.resize(line.size());
		}
		tmp.readFromString(line);
		schedule.push_back(tmp);
	}
	return schedule;
}

bool MapGenerator::generateCurrentMappingFromFile(std::string filename) {
	schedule = readScheduleFromFile(filename);
	return generateCurrentScheduleMapping();
}

void MapGenerator::printCurrentMapping() {
	for (int i = 0; i < currentNightScheduleMapping.size(); i++) {
		std::cout << "Nurse " << i << " Night mapping count: " << currentNightScheduleMapping[i].size()
			<< " | Day mapping count: " << currentDayScheduleMapping[i].size() << std::endl;
	}
}

void MapGenerator::printCurrentSchedule() {
	PatternManager::printPatterns(schedule);
}

void MapGenerator::trimScheduleToLast4Weeks(){
	lastScheduleCopy.clear();
	lastScheduleCopy.resize(16);
	for(int i = 0; i < schedule.size(); i++){
		lastScheduleCopy[i] = schedule[i];
		schedule[i] = schedule[i].getLast4Weeks();
	}
}
