# README #

### How do I get set up? ###

Clone this repository and launch it with Visual Studio. While compiling it
missing packages from NuGet should be downloaded automatically, if it says
you are missing them, enable automatic download of missing packages in NuGet. Tested on Visual Studio 2015

### How it works ###
It waits for a GET message on 9090 port, after that it launches algorithm for generating nurses schedule. After generating it response is sent by http in json format. Also generated 5 weeks and 4 weeks with additional week - 1 are saved to text files in program directory.

### Tests ###
Unit tests are available on repository https://bitbucket.org/Vido4/schedulingtests In order to build tests on certain configuration you have to first launch this program same settings(Debug/Release, x64/x86). Appropriate library to be attached to test program is generated then.