#pragma once
#pragma once
#include <Poco/Net/ServerSocket.h>
#include <Poco/Net/HTTPServer.h>
#include <Poco/Net/HTTPRequestHandler.h>
#include <Poco/Net/HTTPRequestHandlerFactory.h>
#include <Poco/Net/HTTPResponse.h>
#include <Poco/Net/HTTPServerRequest.h>
#include <Poco/Net/HTTPServerResponse.h>
#include <Poco/Util/ServerApplication.h>
#include "Poco/JSON/Parser.h"
#include "Poco/JSON/ParseHandler.h"
#include "Poco/JSON/Stringifier.h"

#include <iostream>
#include <string>
#include <vector>
#include <atomic>
#include <fstream>
#include "JsonParser.h"
#include "MapGenerator.h"

using Poco::JSON::ParseHandler;
using Poco::JSON::Stringifier;
using Poco::JSON::Object;
using Poco::JSON::Parser;
using Poco::Dynamic::Var;
using Poco::DynamicStruct;
using namespace Poco::Net;
using namespace Poco::Util;
using namespace std;

class ConnectionHandler : public HTTPRequestHandler
{
public:
	virtual void handleRequest(HTTPServerRequest &req, HTTPServerResponse &resp)
	{
		if (generating == false) {
			if (req.getMethod() == HTTPRequest::HTTP_GET) {
				generating = true;
				string jsonResponse;
				const char *responseContent;
				size_t responseLen;

				alg.execute();
				jsonResponse = parser.parseSchedule(MapGenerator::lastScheduleCopy);
				saveToFile("4weekschedule.txt", jsonResponse);

				jsonResponse = parser.parseSchedule(MapGenerator::schedule);
				saveToFile("5weekschedule.txt", jsonResponse);
				responseContent = jsonResponse.c_str();
				responseLen = strlen(responseContent);
				resp.setStatus(HTTPResponse::HTTP_OK);
				resp.setContentType("json/html");

				resp.sendBuffer(responseContent, responseLen);
				generating = false;
			}
		}
	}

private:
	void saveToFile(std::string filename, std::string content) {
		ofstream fileOutput;
		fileOutput.open(filename);
		if (!fileOutput.is_open()) {
			std::cout << "Nie udalo sie zapisac do pliku " << filename << std::endl;
			return;
		}
		fileOutput << content;
		fileOutput.close();
	}

	static int count;
	static atomic_bool generating;
	Algorithm alg;
	JsonParser parser;
};

atomic_bool ConnectionHandler::generating = false;
int ConnectionHandler::count = 0;

class MyRequestHandlerFactory : public HTTPRequestHandlerFactory
{
public:
	virtual HTTPRequestHandler* createRequestHandler(const HTTPServerRequest &)
	{
		return new ConnectionHandler;
	}
};

class MyServerApp : public ServerApplication
{
protected:
	int main(const vector<string> &)
	{
		HTTPServer s(new MyRequestHandlerFactory, ServerSocket(9090), new HTTPServerParams);

		s.start();
		cout << endl << "Waiting for generation signal" << endl;

		waitForTerminationRequest();  // wait for CTRL-C or kill

		cout << endl << "Shutting down..." << endl;
		s.stop();

		return 0;
	}
};