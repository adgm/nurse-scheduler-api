#include "SoftPartialConstraints.h"
#include "CombinationsExceptions.h"

int SoftPartialConstraints::calculatePatternCost(ShiftCombination& pattern, std::shared_ptr<Nurse> nurse) {
	if (pattern.getCombinationSize() != 7) {
		throw IncorrectPatternException();
	}
	int shiftSeriesCost = getCostOfShiftsSeries(pattern, nurse);
	if(shiftSeriesCost == 0){
		shiftSeriesCost = getCostOfRestBeforeNights(pattern, nurse);
	}
	return getCostOfIsolatedShifts(pattern)
		+ getCostOfNightSeries(pattern, nurse)
		+ getCostOfShiftsNumberPerWeek(pattern, nurse)
		+ shiftSeriesCost;
}

int SoftPartialConstraints::calculateScheduleCost(ShiftCombination& pattern, std::shared_ptr<Nurse> nurse) {
	return getCostOfIsolatedShifts(pattern)
		+ getCostOfNightSeries(pattern, nurse)
		+ getCostOfShiftsNumberPerWeek(pattern, nurse)
		+ getCostOfShiftsSeries(pattern, nurse);
}

int SoftPartialConstraints::calculateDivisionCost(ShiftCombination& pattern, int currentDay){
	return getCostOfEarlyAfterDayShift(pattern, currentDay)
		+ getCostOfEarlyShiftSeries(pattern, currentDay);
}

int SoftPartialConstraints::getCostOfIsolatedShifts(ShiftCombination& shifts) {
	int isolatedCount = 0;
	for (int i = 1; i < shifts.getCombinationSize() - 1; i++) {
		if (shifts[i] != ShiftCombination::ShiftType::REST
			&& shifts[i - 1] == ShiftCombination::ShiftType::REST
			&& shifts[i + 1] == ShiftCombination::ShiftType::REST) {
			isolatedCount++;
		}
	}
	return isolatedCount * 1000;
}

int SoftPartialConstraints::getCostOfNightSeries(ShiftCombination& shifts,
	std::shared_ptr<Nurse> nurse) {
	int isolatedNights = 0;
	if (nurse->getHoursRange() == Nurse::LOWHOURS) {
		for (int i = 1; i < shifts.getCombinationSize() - 1; i++) {
			if (shifts[i] == ShiftCombination::ShiftType::NIGHT
				&& shifts[i - 1] != ShiftCombination::ShiftType::NIGHT
				&& shifts[i + 1] != ShiftCombination::ShiftType::NIGHT) {
				isolatedNights++;
			}
		}
	}
	return isolatedNights * 1000;
}

int SoftPartialConstraints::getCostOfShiftsNumberPerWeek(ShiftCombination& shifts,
	std::shared_ptr<Nurse> nurse) {
	size_t weeks = shifts.getCombinationSize() / WEEK_LENGTH;
	int cost = 0;
	if (nurse->getHoursRange() != Nurse::LOWHOURS) {
		for (int i = 0; i < weeks; i++) {
			int shiftPerWeekCount = 0;
			for (int j = 0; j < WEEK_LENGTH; j++) {
				if (ShiftCombination::ShiftType::REST != shifts[i * WEEK_LENGTH + j]) {
					shiftPerWeekCount++;
				}
			}
			if (shiftPerWeekCount < 4) {
				cost += (4 - shiftPerWeekCount) * 10;
			}
			if (shiftPerWeekCount > 5){
				cost += (shiftPerWeekCount - 5) * 10;
			}
		}
		return cost;
	}
	return 0;
}
/*
 * Special case of getCostOfShiftsSeries - it will incur cost in getCostOfShiftsSeries constraint but when adding
 * another pattern to this one - as separate pattern function getCostOfShiftsSeries doesn't return cost
 */
int SoftPartialConstraints::getCostOfRestBeforeNights(ShiftCombination& shifts,
	std::shared_ptr<Nurse> nurse){
	int nightSeriesCount = 0;
	int nightCount = 0;
	if (nurse->getHoursRange() != Nurse::LOWHOURS) {
		if(shifts[shifts.getCombinationSize() - 1] == ShiftCombination::ShiftType::NIGHT){
			if(shifts[shifts.getCombinationSize() - 2] != ShiftCombination::ShiftType::REST
				&& shifts[shifts.getCombinationSize() - 3] != ShiftCombination::ShiftType::REST
				&& shifts[shifts.getCombinationSize() - 4] != ShiftCombination::ShiftType::REST){
				return 0;
			}
			return 10;
		}
	}
	return 0;
}

int SoftPartialConstraints::getCostOfEarlyAfterDayShift(ShiftCombination& shifts, int currentDay){
	int countOfcosts = 0;
	if(currentDay == -1){
		currentDay = shifts.getExtendedCombinationSize() - 1;
	}
	for(int i = 1; i <= currentDay; i++){
		if(shifts.getExtendedShift(i) == ShiftCombination::ExtendedShiftType::EARLY
			&& shifts.getExtendedShift(i-1) == ShiftCombination::ExtendedShiftType::MID){
			countOfcosts++;
		}
	}
	return countOfcosts * 5;
}

int SoftPartialConstraints::getCostOfEarlyShiftSeries(ShiftCombination& shifts, int currentDay){
	if (currentDay == -1) {
		currentDay = shifts.getExtendedCombinationSize() - 1;
	}
	bool fromStart = true;
	int cost = 0;
	int currentCount = 0;
	for (int i = 0; i <= currentDay; i++) {
		if (shifts.getExtendedShift(i) == ShiftCombination::ExtendedShiftType::EARLY) {
			currentCount++;
		}else{
			if(currentCount != 0 && currentCount < 2 || currentCount > 3){
				if (currentCount < 2 && !fromStart) {
					cost += (2 - currentCount) * 10;
				}else if(currentCount > 3){
					cost += (currentCount - 3) * 10;
				}
			}
			currentCount = 0;
			fromStart = false;
		}
	}
	return cost;
}

int SoftPartialConstraints::getCostOfShiftsSeries(ShiftCombination& shifts,
	std::shared_ptr<Nurse> nurse) {
	int shiftSeriesCount = 0;
	int seriesStart = -1;
	int cost = 0;
	if (nurse->getHoursRange() != Nurse::LOWHOURS) {
		for (int i = 0; i < shifts.getCombinationSize(); i++) {
			if (ShiftCombination::ShiftType::REST != shifts[i]) {
				if (shiftSeriesCount == 0) {
					seriesStart = i;
				}
				shiftSeriesCount++;
			}else{
				if(seriesStart > 0 && shiftSeriesCount > 0){
					if (shiftSeriesCount < 4) {
						cost += (4 - shiftSeriesCount) * 10;
					}
					else if(shiftSeriesCount > 5){
						cost += (shiftSeriesCount - 5) * 10;
					}
				}
				if(seriesStart == 0 && shiftSeriesCount > 5){
					cost += (shiftSeriesCount - 5) * 10;
				}
				shiftSeriesCount = 0;
			}
		}
	}
	if (shiftSeriesCount > 5 
		&& ShiftCombination::ShiftType::REST != shifts[shifts.getCombinationSize() - 1]) {
			cost += (shiftSeriesCount - 5) * 10;
	}
	return cost;
}
