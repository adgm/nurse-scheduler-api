#pragma once
#include <exception>

class IncorrectPatternException : public std::exception
{
	virtual const char* what() const throw() override
	{
		return "Wrong size of Pattern!";
	}
};

class IncorrectDayException : public std::exception
{
	virtual const char* what() const throw() override
	{
		return "Incorrect day of combination!";
	}
};

class IncorrectShiftTypeException : public std::exception
{
	virtual const char* what() const throw() override
	{
		return "Incorrect shift type!";
	}
};

class IncorrectNurseHourRange : public std::exception
{
	virtual const char* what() const throw() override
	{
		return "Incorrect nurse hour range!";
	}
};