#include "HelperConstraints.h"

bool HelperConstraints::checkPatternValidity(ShiftCombination& pattern, std::shared_ptr<Nurse> nurse) {
	return hasNoIsolatedNightShiftOnEdge(pattern, nurse) && hasProperShiftNumberPerWeek(pattern, nurse);
}

bool HelperConstraints::hasNoIsolatedNightShiftOnEdge(ShiftCombination& shifts, std::shared_ptr<Nurse> nurse) {
	if (nurse->getHoursRange() == Nurse::LOWHOURS) {
		for (int i = 1; i < shifts.getCombinationSize() - 1; i++) {
			if ((shifts[0] == ShiftCombination::ShiftType::NIGHT
				&& shifts[1] != ShiftCombination::ShiftType::NIGHT)
				|| (shifts[shifts.getCombinationSize() - 1] == ShiftCombination::ShiftType::NIGHT
					&& shifts[shifts.getCombinationSize() - 2] != ShiftCombination::ShiftType::NIGHT)) {
				return false;
			}
		}
	}
	return true;
}

bool HelperConstraints::hasProperShiftNumberPerWeek(ShiftCombination& shifts, std::shared_ptr<Nurse> nurse) {
	int shiftLowPerWeekCount = 0;
	if (nurse->getHoursRange() == Nurse::LOWHOURS) {
		for (int i = 0; i < shifts.getCombinationSize(); i++) {
			if (ShiftCombination::ShiftType::REST != shifts[i]) {
				shiftLowPerWeekCount++;
			}
		}
		if (shiftLowPerWeekCount < 2 || shiftLowPerWeekCount > 3) {
			return false;
		}
	}
	return true;
}

bool HelperConstraints::hasLessThanGivenFreeWeekendsPerSchedule(ShiftCombination& shifts, int given) {
	int freeWeeksCount = 0;
	for (int i = 4; i < shifts.getCombinationSize(); i += 7) {
		if (shifts[i] != ShiftCombination::ShiftType::NIGHT
			&& shifts[i + 1] == ShiftCombination::ShiftType::REST
			&& shifts[i + 2] == ShiftCombination::ShiftType::REST) {
			freeWeeksCount++;
		}
	}
	return freeWeeksCount < given;
}

bool HelperConstraints::hasFreeWeekInPattern(ShiftCombination& shifts) {
	if (shifts.getCombinationSize() != 7) {
		throw IncorrectPatternException();
	}
	if (shifts[4] != ShiftCombination::ShiftType::NIGHT
		&& shifts[5] == ShiftCombination::ShiftType::REST
		&& shifts[6] == ShiftCombination::ShiftType::REST) {
		return true;
	}
	return false;
}

bool HelperConstraints::isValidShiftNumber(ShiftCombination& shifts, std::shared_ptr<Nurse> nurse) {
	int weeks = shifts.getCombinationSize() / WEEK_LENGTH;
	int overload;
	int underload;
	int upperShiftsCount;
	int lowerShiftsCount;
	if (nurse->getHoursRange() != Nurse::LOWHOURS) {
		upperShiftsCount = 5;
		lowerShiftsCount = 4;
		overload = shifts.getWorkingShiftsCount() - weeks * 5;
		underload = weeks * 4 - shifts.getWorkingShiftsCount();
	}else{
		upperShiftsCount = 3;
		lowerShiftsCount = 2;
		overload = shifts.getWorkingShiftsCount() - weeks * 3;
		underload = weeks * 2 - shifts.getWorkingShiftsCount();
	}
	if(overload > 0){
		return false;
	}
	int workdays = 0;
	for (int i = shifts.getCombinationSize() - WEEK_LENGTH; i < shifts.getCombinationSize(); i++) {
		if (ShiftCombination::ShiftType::REST != shifts[i]) {
			workdays++;
		}
	}
	if(underload > 0){
		return workdays == 6;
	}
	if(underload == 0 && workdays == 6){
		return true;
	}
	return workdays <= upperShiftsCount && workdays >= lowerShiftsCount;
}