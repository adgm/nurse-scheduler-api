#pragma once
#include "Nurse.h"
#include "HardPartialConstraints.h"
#include "SoftPartialConstraints.h"
#include "HelperConstraints.h"

class ScheduleChecker {
public:
	static int getCostOfNursePartialSchedule(std::shared_ptr<Nurse> nurse) {
		ShiftCombination combination = nurse->getShifts();
		return getCostOfPartialSchedule(nurse, combination);
	}

	static int getCostOfInitialSchedule(std::shared_ptr<Nurse> nurse, ShiftCombination& combination) {
		if (hardConstraint.checkScheduleValidity(combination, nurse)) {
			return softConstraint.calculateScheduleCost(combination, nurse);
		}
		return -1;
	}

	static int getCostOfPartialSchedule(std::shared_ptr<Nurse> nurse, ShiftCombination& combination) {
		if (hardConstraint.checkScheduleValidity(combination, nurse)
			&& HelperConstraints::isValidShiftNumber(combination, nurse)) {
			return softConstraint.calculateScheduleCost(combination, nurse);
		}
		return -1;
	}

	static int getCostOfPattern(ShiftCombination& combination, std::shared_ptr<Nurse> nurse) {
		if (hardConstraint.checkPatternValidity(combination)) {
			return softConstraint.calculatePatternCost(combination, nurse);
		}
		return -1;
	}

	static bool satisfiesHelperConstraints(ShiftCombination& combination, std::shared_ptr<Nurse> nurse){
		return helperConstraint.checkPatternValidity(combination, nurse);
	}
private:
	static SoftPartialConstraints softConstraint;
	static HardPartialConstraints hardConstraint;
	static HelperConstraints helperConstraint;
};
