#include "PatternSelector.h"
#include <algorithm>
#include "Algorithm.h"

int PatternSelector::selectSuggestedBalancedPatternWithDemand(std::shared_ptr<Nurse> nurse, std::vector<ShiftCombination> &vect,
	int week, int daySuggestion) {
	int numberOfShifts = selectBalancedPatternWithDemand(nurse, vect, week, daySuggestion);
	if (numberOfShifts < 1) {
		return selectBalancedPatternWithDemand(nurse, vect, week, 0);
	}
	return numberOfShifts;
}

int PatternSelector::selectSuggestedBalancedPatternWithoutDemand(std::shared_ptr<Nurse> nurse, std::vector<ShiftCombination> &vect,
	int week, int daySuggestion) {
	int numberOfShifts = selectBalancedPatternWithoutDemand(nurse, vect, week, daySuggestion);
	if (numberOfShifts < 1) {
		return  selectBalancedPatternWithoutDemand(nurse, vect, week, 0);
	}
	return numberOfShifts;
}

int PatternSelector::selectBalancedPatternWithDemand(std::shared_ptr<Nurse> nurse, std::vector<ShiftCombination> &vect,
	int week, int daySuggestion) {
		for (ShiftCombination &combination : vect) {
				if (patternFulfillsRequirements(combination, week)) {
					int shiftsCount = selectSuggestedBalancedPattern(nurse, combination, week, daySuggestion);
					if (shiftsCount > 0) {
						return shiftsCount;
					}
				}
		}
	return 0;
}

int PatternSelector::selectBalancedPatternWithoutDemand(std::shared_ptr<Nurse> nurse, std::vector<ShiftCombination> &vect,
	int week, int daySuggestion) {
		for (ShiftCombination &combination : vect) {
				int shiftsCount = selectSuggestedBalancedPattern(nurse, combination, week, daySuggestion);
				if (shiftsCount > 0) {
					return shiftsCount;
				}
			}
	return 0;
}

int PatternSelector::selectSuggestedBalancedPattern(std::shared_ptr<Nurse> nurse, ShiftCombination &combination,
	int week, int daySuggestion) {
	if (daySuggestion == 0 || (combination.getWorkingShiftsCount()) == daySuggestion) {
		int shiftsCount = selectBalancedPattern(nurse, combination, week);
		if (shiftsCount > 0) {
			return shiftsCount;
		}
	}
	return 0;
}

int PatternSelector::selectBalancedPattern(std::shared_ptr<Nurse> nurse, ShiftCombination& combination, int week) {
	int shiftsCount = selectPatternWithCost(nurse, combination, week, 0);
	if (shiftsCount > 0 && isDemandBalanced(nurse, combination, week)) {
		adjustDayAndNightRequirements(combination, week);
		return shiftsCount;
	}

	return 0;
}

int PatternSelector::selectSuggestedUnbalancedPatternWithDemand(std::shared_ptr<Nurse> nurse, std::vector<ShiftCombination> &vect,
	int week, int daySuggestion, bool fix) {
	int numberOfShifts = selectUnbalancedPatternWithDemand(nurse, vect, week, daySuggestion, fix);
	if (numberOfShifts < 1) {
		return selectUnbalancedPatternWithDemand(nurse, vect, week, 0, fix);
	}
	return numberOfShifts;
}

int PatternSelector::selectUnbalancedPatternWithDemand(std::shared_ptr<Nurse> nurse, std::vector<ShiftCombination> &vect,
	int week, int daySuggestion, bool fix) {
		for (ShiftCombination &combination : vect) {
				if (patternFulfillsRequirements(combination, week)) {
					int shiftsCount = selectSuggestedUnbalancedPattern(nurse, combination, week, daySuggestion, fix);
					if (shiftsCount > 0) {
						return shiftsCount;
					}
				}
			}
	return 0;
}

int PatternSelector::selectSuggestedUnbalancedPatternWithoutDemand(std::shared_ptr<Nurse> nurse, std::vector<ShiftCombination> &vect,
	int week, int daySuggestion) {
	int numberOfShifts = selectUnbalancedPatternWithoutDemand(nurse, vect, week, daySuggestion);
	if (numberOfShifts < 1) {
		return selectUnbalancedPatternWithoutDemand(nurse, vect, week, 0);
	}
	return numberOfShifts;
}

int PatternSelector::selectUnbalancedPatternWithoutDemand(std::shared_ptr<Nurse> nurse, std::vector<ShiftCombination> &vect,
	int week, int daySuggestion) {
		for (ShiftCombination &combination : vect) {
				int shiftsCount = selectSuggestedUnbalancedPattern(nurse, combination, week, daySuggestion);
				if (shiftsCount > 0) {
					return shiftsCount;
				}
			}
	return 0;
}

int PatternSelector::selectSuggestedUnbalancedPattern(std::shared_ptr<Nurse> nurse, ShiftCombination &combination,
	int week, int daySuggestion, bool fix) {
	if (daySuggestion == 0 || (combination.getWorkingShiftsCount()) == daySuggestion) {
		int shiftsCount = selectUnbalancedPattern(nurse, combination, week, fix);
		if (shiftsCount > 0) {
			return shiftsCount;
		}
	}
	return 0;
}

int PatternSelector::selectUnbalancedPattern(std::shared_ptr<Nurse> nurse, ShiftCombination& combination, int week, bool fix) {
	ShiftCombination patternToBeChanged = nurse->getShifts();
	int shiftsCount = selectPatternWithCost(nurse, combination, week, 0);
	if (shiftsCount > 0) {
		if(fix){
			retractDayAndNightRequirements(patternToBeChanged, week);
		}
		adjustDayAndNightRequirements(combination, week);
		return shiftsCount;
	}
	return 0;
}

void PatternSelector::retractDayAndNightRequirements(ShiftCombination &pattern, int week) {
	for (int i = WEEK_LENGTH * week; i < WEEK_LENGTH * week + WEEK_LENGTH; i++) {
		if (pattern[i] == ShiftCombination::ShiftType::DAY) {
			dayRequirements[i]++;
		}
		if (pattern[i] == ShiftCombination::ShiftType::NIGHT) {
			nightRequirements[i]++;
		}
	}
}

int PatternSelector::selectPatternWithCost(std::shared_ptr<Nurse> nurse, ShiftCombination& combination, int week, int cost) const{
	updateCombination(nurse, combination, week);
	if (ScheduleChecker::getCostOfNursePartialSchedule(nurse) == cost) {
		return combination.getWorkingShiftsCount();
	}
	return 0;
}

void PatternSelector::updateCombination(std::shared_ptr<Nurse> nurse, ShiftCombination &pattern, int week) {
	if (nurse->getShifts().getCombinationSize() < (week + 1) * WEEK_LENGTH) {
		nurse->appendPattern(pattern);
	}
	else {
		nurse->replaceLastWeekPattern(pattern);
	}
}

void PatternSelector::adjustRequirements(std::array<int, WEEK_LENGTH> &requirements,
	ShiftCombination &pattern, ShiftCombination::ShiftType type){
	for (int i = 0; i < WEEK_LENGTH; i++) {
		if (pattern[i] == type)
			requirements[i]--;
	}
}

void PatternSelector::adjustRequirements(std::array<int, SCHEDULE_LENGTH> &requirements,
	ShiftCombination &pattern, ShiftCombination::ShiftType type, int week) {
	for (int i = 0; i < WEEK_LENGTH; i++) {
		if (pattern[i] == type)
			requirements[i + week * WEEK_LENGTH]--;
	}
}

void PatternSelector::adjustDayAndNightRequirements(ShiftCombination &pattern, int week) {
	adjustRequirements(dayRequirements, pattern, ShiftCombination::ShiftType::DAY, week);
	adjustRequirements(nightRequirements, pattern, ShiftCombination::ShiftType::NIGHT, week);
}

std::array<int, SCHEDULE_LENGTH> PatternSelector::getDaysToAdd() {
	return getShiftsToAdd(dayRequirements);
}

std::array<int, SCHEDULE_LENGTH> PatternSelector::getNightsToAdd() {
	return getShiftsToAdd(nightRequirements);
}

std::array<int, SCHEDULE_LENGTH> PatternSelector::getShiftsToAdd(std::array<int, SCHEDULE_LENGTH>& requirements) {
	std::array<int, SCHEDULE_LENGTH> daysToAdd = { 0 };
	for (int i = 0; i < SCHEDULE_LENGTH; i++) {
		if (requirements[i] > 0) {
			daysToAdd[i] = requirements[i];
		}
	}
	return daysToAdd;
}

std::array<int, SCHEDULE_LENGTH> PatternSelector::getDaysToErase() {
	return getShiftsToErase(dayRequirements);
}

std::array<int, SCHEDULE_LENGTH> PatternSelector::getNightsToErase() {
	return getShiftsToErase(nightRequirements);
}

std::array<int, SCHEDULE_LENGTH> PatternSelector::getShiftsToErase(std::array<int, SCHEDULE_LENGTH>& requirements){
	std::array<int, SCHEDULE_LENGTH> daysToErase = { 0 };
	for (int i = 0; i < SCHEDULE_LENGTH; i++) {
		if (requirements[i] < 0) {
			daysToErase[i] = abs(requirements[i]);
		}
	}
	return daysToErase;
}

int PatternSelector::getAmountOfDayShiftsLeft(int week) const{
	std::array<int, WEEK_LENGTH> weeklyDaysRequirements = getRequirementsForWeek(dayRequirements, week);
	std::array<int, WEEK_LENGTH> weeklyNightsRequirements = getRequirementsForWeek(nightRequirements, week);
	int sum = 0;
	for (int &i : weeklyDaysRequirements) {
		if (i > 0) {
			sum += i;
		}
	}
	for (int &i : weeklyNightsRequirements) {
		if (i > 0) {
			sum += i;
		}
	}
	return sum;
}

void PatternSelector::clearRequirements() {
	std::array<int, WEEK_LENGTH> weeklyDaysRequirements = { 9,9,9,9,9,6,6 };
	std::array<int, WEEK_LENGTH> weeklyNightsRequirements = { 1,1,1,1,1,1,1 };
	for (int i = 0; i < SCHEDULE_WEEKS; i++) {
		copy(weeklyDaysRequirements.begin(), weeklyDaysRequirements.begin() + WEEK_LENGTH, dayRequirements.begin() + i * WEEK_LENGTH);
		copy(weeklyNightsRequirements.begin(), weeklyNightsRequirements.begin() + WEEK_LENGTH, nightRequirements.begin() + i * WEEK_LENGTH);
	}
}

bool PatternSelector::isDemandBalanced(std::shared_ptr<Nurse> nurse, ShiftCombination &combination, int week) {
	std::array<int, WEEK_LENGTH> newDayRequirements = getRequirementsForWeek(dayRequirements, week);
	std::array<int, WEEK_LENGTH> newNightRequirements = getRequirementsForWeek(nightRequirements, week);
	adjustRequirements(newDayRequirements, combination, ShiftCombination::ShiftType::DAY);
	adjustRequirements(newNightRequirements, combination, ShiftCombination::ShiftType::NIGHT);

	return isMaxChangedInCombination(combination, week)
		&& isMaxDifferenceSameOrLower(newDayRequirements, week)
		&& hasNoNegativeImpactOnDemand(newDayRequirements, newNightRequirements, week)
		&& isNotCompletelyIsolated(newDayRequirements)
		&& checkIfFreeWeekend(nurse, combination, week);
}

std::array<int, WEEK_LENGTH> PatternSelector::getRequirementsForWeek(std::array<int, SCHEDULE_LENGTH> requirements ,int week) const{
	std::array<int, WEEK_LENGTH> weeklyRequirements;
	copy(requirements.begin() + week * WEEK_LENGTH, requirements.begin() + week * WEEK_LENGTH + WEEK_LENGTH, weeklyRequirements.begin());
	return weeklyRequirements;
}

bool PatternSelector::isMaxChangedInCombination(ShiftCombination &combination, int week) {
	std::array<int, WEEK_LENGTH> currentDayRequirements = getRequirementsForWeek(dayRequirements, week);
	int maxValue = getMaxValue(currentDayRequirements);
	for (int i = 0; i < currentDayRequirements.size(); i++) {
		if (currentDayRequirements[i] == maxValue) {
			if (combination[i] != ShiftCombination::ShiftType::REST) {
				return true;
			}
		}
	}
	return false;
}

bool PatternSelector::isMaxDifferenceSameOrLower(std::array<int, WEEK_LENGTH> &newDayRequirement, int week) const{
	std::array<int, WEEK_LENGTH> dayWeeklyRequirements = getRequirementsForWeek(dayRequirements, week);

	int prevdiff = getBiggestDayDifference(dayWeeklyRequirements);
	int currdiff = getBiggestDayDifference(newDayRequirement);

	if (prevdiff == 0) {
		return currdiff < 2;//Need to allow some difference in order for pattern to progress
	}
	if (prevdiff == 1) {
		return currdiff < 3;
	}
	return currdiff <= prevdiff;
}

bool PatternSelector::hasNoNegativeImpactOnDemand(std::array<int, WEEK_LENGTH> &newNightRequirements,
	std::array<int, WEEK_LENGTH> &newDayRequirements, int week) const{
	
	std::array<int, WEEK_LENGTH> dayWeeklyRequirements = getRequirementsForWeek(dayRequirements, week);
	std::array<int, WEEK_LENGTH> nightWeeklyRequirements = getRequirementsForWeek(nightRequirements, week);

	int currentSumValue = getSumValue(dayWeeklyRequirements) + getSumValue(nightWeeklyRequirements);
	int newSumValue = getSumValue(newDayRequirements) + getSumValue(newNightRequirements);
	if(currentSumValue < newSumValue){
		return false;
	}
	return true;
}

bool PatternSelector::isNotCompletelyIsolated(std::array<int, WEEK_LENGTH> newDayRequirements) {
	for (int i = 1; i < WEEK_LENGTH - 1; i++) {
		if (newDayRequirements[i] > newDayRequirements[i - 1] + 2
			&& newDayRequirements[i] > newDayRequirements[i + 1] + 2) {
			return false;
		}
	}
	return true;
}


bool PatternSelector::checkIfFreeWeekend(std::shared_ptr<Nurse> nurse, ShiftCombination &combination, int week) const {
//	if(week > 0 && week < 4){
//		return !helperConstraint->hasLessThanGivenFreeWeekendsPerSchedule(combination, 1);
//	}
//	if(week == 4){
//		return !helperConstraint->hasLessThanGivenFreeWeekendsPerSchedule(combination, 2);
//	}
	return true;
}

int PatternSelector::getSumValue(std::array<int, WEEK_LENGTH> &requirements) const{
	int sum = 0;
	for(int &day : requirements){
		sum += abs(day);
	}
	return sum;
}

int PatternSelector::getMaxValue(std::array<int, WEEK_LENGTH> &dayRequirements) {
	int max = 0;
	for (int &i : dayRequirements) {
		if (max < i) {
			max = i;
		}
	}
	return max;
}

int PatternSelector::getBiggestDayDifference(std::array<int, WEEK_LENGTH> &dayRequirements) {
	return getDayDifference(0, 7, dayRequirements);
}

int PatternSelector::getDayDifference(int from, int to, std::array<int, WEEK_LENGTH> &dayRequirements) {
	int maxDay = dayRequirements[from];
	int minDay = dayRequirements[from];
	for (int i = from + 1; i < to; i++) {
		if (dayRequirements[i] > maxDay) {
			maxDay = dayRequirements[i];
		}
		if (dayRequirements[i] < minDay) {
			minDay = dayRequirements[i];
		}
	}
	return maxDay - minDay;
}

bool PatternSelector::patternFulfillsRequirements(ShiftCombination &pattern, int week) const{
	std::array<int, WEEK_LENGTH> dayWeeklyRequirements = getRequirementsForWeek(dayRequirements, week);
	std::array<int, WEEK_LENGTH> nightWeeklyRequirements = getRequirementsForWeek(nightRequirements, week);
	for (int i = 0; i < WEEK_LENGTH; i++) {
		switch (pattern[i]) {
		case ShiftCombination::ShiftType::DAY:
			if (dayWeeklyRequirements[i] < 1) {
				return false;
			}
			break;
		case ShiftCombination::ShiftType::NIGHT:
			if (nightWeeklyRequirements[i] < 1) {
				return false;
			}
			break;
		}
	}
	return true;
}

void PatternSelector::printDebug(int nurse, int shiftsUsed, int week) {
	std::cout << "NURSE " << nurse << "---" << std::endl;
	std::cout << "Shifts Used: " << shiftsUsed << std::endl;
	std::cout << "Day Demand: ";
	std::array<int, WEEK_LENGTH> weeklyDayRequirements = getRequirementsForWeek(dayRequirements, week);
	std::array<int, WEEK_LENGTH> weeklyNightRequirements = getRequirementsForWeek(nightRequirements, week);
	for (int &i : weeklyDayRequirements) {
		std::cout << i << ",";
	}
	std::cout << std::endl;
	std::cout << "Night Demand: " << std::endl;
	for (int &i : weeklyNightRequirements) {
		std::cout << i << ",";
	}
	std::cout << std::endl << std::endl;
}