#pragma once
#include "ShiftCombination.h"
#include <memory>

class Nurse
{
public:
	explicit Nurse(int ID, int hours, bool allowLateShifts) 
	: ID(ID), hoursPerWeek(hours), nightShiftsAllowed(allowLateShifts){
		shifts = std::make_shared<ShiftCombination>(0);
	}

	enum NurseHoursRange {
		LOWHOURS,
		MIDHOURS,
		HIGHHOURS
	};

	NurseHoursRange getHoursRange() const{
		if(hoursPerWeek > 32){
			return HIGHHOURS;
		}
		if(hoursPerWeek <= 20){
			return LOWHOURS;
		}
		return MIDHOURS;
	}

	bool allowNightShifts() const{ return nightShiftsAllowed; }

	int getID() const{
		return ID;
	}

	ShiftCombination getShifts() const{
		return *shifts;
	}

	void appendPattern(ShiftCombination& pattern) const{
		shifts->appendCombination(pattern);
	}

	void replaceLastWeekPattern(ShiftCombination& pattern) const{
		shifts->replaceLastWeekShifts(pattern);
	}

	void replaceSpecificShift(int shiftDay, ShiftCombination::ShiftType shift) const{
		shifts->changeShiftOnDay(shiftDay, shift);
	}

private:
	const int ID;
	int hoursPerWeek;
	bool nightShiftsAllowed;
	std::shared_ptr<ShiftCombination> shifts;
};
