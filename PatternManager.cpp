#include "PatternManager.h"
#include <numeric>
#include <set>

std::vector<ShiftCombination> PatternManager::noCostNightPatternsLowHours;
std::vector<ShiftCombination> PatternManager::noCostDayPatternsLowHours;

std::vector<ShiftCombination> PatternManager::noCostNightPatternsHighHours;
std::vector<ShiftCombination> PatternManager::noCostDayPatternsHighHours;

std::vector<ShiftCombination> PatternManager::lowCostNightPatternsLowHours;
std::vector<ShiftCombination> PatternManager::lowCostDayPatternsLowHours;

std::vector<ShiftCombination> PatternManager::lowCostNightPatternsHighHours;
std::vector<ShiftCombination> PatternManager::lowCostDayPatternsHighHours;

std::vector<std::pair<int, std::vector<ShiftCombination>>> PatternManager::nightSets;

void PatternManager::generatePatterns() {
	iterateCombinationsWithDepth(7, ShiftCombination::ShiftType::END);
	auto compareWeekends = [](ShiftCombination &a, ShiftCombination &b) -> bool {
		return b.hasFreeWeekend() < a.hasFreeWeekend();
	};

	auto compareDays = [](ShiftCombination &a, ShiftCombination &b) -> bool {
		if (b.hasFreeWeekend() == a.hasFreeWeekend()) {
			return b.getDayShiftsCount() < a.getDayShiftsCount();
		};
		return false;
	};
	std::sort(noCostDayPatternsLowHours.begin(), noCostDayPatternsLowHours.end(), compareWeekends);
	std::sort(noCostDayPatternsHighHours.begin(), noCostDayPatternsHighHours.end(), compareWeekends);
	std::sort(lowCostDayPatternsLowHours.begin(), lowCostDayPatternsLowHours.end(), compareWeekends);
	std::sort(lowCostDayPatternsHighHours.begin(), lowCostDayPatternsHighHours.end(), compareWeekends);

	std::sort(noCostDayPatternsLowHours.begin(), noCostDayPatternsLowHours.end(), compareDays);
	std::sort(noCostDayPatternsHighHours.begin(), noCostDayPatternsHighHours.end(), compareDays);
	std::sort(lowCostDayPatternsLowHours.begin(), lowCostDayPatternsLowHours.end(), compareDays);
	std::sort(lowCostDayPatternsHighHours.begin(), lowCostDayPatternsHighHours.end(), compareDays);

	auto compareNights = [](ShiftCombination &a, ShiftCombination &b) -> bool {
		if (b.hasFreeWeekend() == a.hasFreeWeekend()) {
			return b.getNightShiftsCount() < a.getNightShiftsCount();
		}
		return false;
	};
	std::sort(noCostNightPatternsLowHours.begin(), noCostNightPatternsLowHours.end(), compareWeekends);
	std::sort(noCostNightPatternsHighHours.begin(), noCostNightPatternsHighHours.end(), compareWeekends);
	std::sort(lowCostNightPatternsLowHours.begin(), lowCostNightPatternsLowHours.end(), compareWeekends);
	std::sort(lowCostNightPatternsHighHours.begin(), lowCostNightPatternsHighHours.end(), compareWeekends);

	std::sort(noCostNightPatternsLowHours.begin(), noCostNightPatternsLowHours.end(), compareNights);
	std::sort(noCostNightPatternsHighHours.begin(), noCostNightPatternsHighHours.end(), compareNights);
	std::sort(lowCostNightPatternsLowHours.begin(), lowCostNightPatternsLowHours.end(), compareNights);
	std::sort(lowCostNightPatternsHighHours.begin(), lowCostNightPatternsHighHours.end(), compareNights);
}

void PatternManager::iterateCombinationsWithDepth(int depth, ShiftCombination::ShiftType maxShiftValue) {
	ShiftCombination pattern;
	int count = 0;
	int index = 0;

	while (true) {
		count++;
		addPatternIfEligible(pattern);
		incrementShiftOnDayInPattern(pattern, index);

		while (pattern[index] == maxShiftValue) {
			if (index == depth - 1) {
				return;
			}
			pattern.changeShiftOnDay(index, ShiftCombination::ShiftType::REST);
			index++;
			incrementShiftOnDayInPattern(pattern, index);
		}
		index = 0;
	}
}

void PatternManager::addPatternIfEligible(ShiftCombination &pattern) {
	std::shared_ptr<Nurse> lowHourNurse = std::make_shared<Nurse>(0, 20, true);
	std::shared_ptr<Nurse> highHourNurse = std::make_shared<Nurse>(1, 36, true);
	addPatternTo(lowHourNurse, pattern, noCostDayPatternsLowHours, noCostNightPatternsLowHours, 0);
	addPatternTo(highHourNurse, pattern, noCostDayPatternsHighHours, noCostNightPatternsHighHours, 0);
	addPatternTo(lowHourNurse, pattern, lowCostDayPatternsLowHours, lowCostNightPatternsLowHours, 30);
	addPatternTo(highHourNurse, pattern, lowCostDayPatternsHighHours, lowCostNightPatternsHighHours, 30);
}

void PatternManager::addPatternTo(std::shared_ptr<Nurse> nurse, ShiftCombination &pattern,
	std::vector<ShiftCombination> &dayVect, std::vector<ShiftCombination> &nightVect, int cost,
	bool satisfyHelper) const {
	int currentcost = ScheduleChecker::getCostOfPattern(pattern, nurse);
	if (satisfyHelper && !ScheduleChecker::satisfiesHelperConstraints(pattern, nurse)) {
		return;
	}

	if ((cost == 0 && currentcost == cost)
		|| (cost > 0 && currentcost <= cost && currentcost > 0)) {
		if (pattern.getNightShiftsCount() > 0) {
			nightVect.push_back(pattern);
		}
		else {
			dayVect.push_back(pattern);
		}
	}
}

void PatternManager::incrementShiftOnDayInPattern(ShiftCombination &pattern, int index) {
	pattern.changeShiftOnDay(index, static_cast<ShiftCombination::ShiftType>(pattern[index] + 1));
}

void PatternManager::printPatterns() {
	std::cout << "noCostNightPatternsLowHours: " << std::endl;
	printPatterns(noCostNightPatternsLowHours);
	std::cout << std::endl << "noCostDayPatternsLowHours: " << std::endl;
	printPatterns(noCostDayPatternsLowHours);
	std::cout << "noCostNightPatternsHighHours: " << std::endl;
	printPatterns(noCostNightPatternsHighHours);
	std::cout << std::endl << "noCostDayPatternsHighHours: " << std::endl;
	printPatterns(noCostDayPatternsHighHours);

	std::cout << "--------------------------------Cost 20" << std::endl;
	std::cout << "lowCostNightPatternsLowHours: " << std::endl;
	printPatterns(lowCostNightPatternsLowHours);
	std::cout << std::endl << "lowCostDayPatternsLowHours: " << std::endl;
	printPatterns(lowCostDayPatternsLowHours);
	std::cout << "lowCostNightPatternsHighHours: " << std::endl;
	printPatterns(lowCostNightPatternsHighHours);
	std::cout << std::endl << "lowCostDayPatternsHighHours: " << std::endl;
	printPatterns(lowCostDayPatternsHighHours);
}

void PatternManager::generateNightSets(bool withCost) {
	std::array<int, WEEK_LENGTH> weeklyNightDemand = { 1,1,1,1,1,1,1 };
	std::array<int, WEEK_LENGTH> counters = { -1,-1,-1,-1,-1,-1,-1 };
	std::pair<int, std::vector<ShiftCombination>> nightSetWithLowHourCount;
	std::vector<ShiftCombination> noCostNightPatterns;
	noCostNightPatterns.reserve(noCostNightPatternsHighHours.size() + noCostNightPatternsLowHours.size());
	noCostNightPatterns.insert(noCostNightPatterns.end(), noCostNightPatternsHighHours.begin(),
		noCostNightPatternsHighHours.end());
	noCostNightPatterns.insert(noCostNightPatterns.end(), noCostNightPatternsLowHours.begin(),
		noCostNightPatternsLowHours.end());

	bool running = true;
	int j = 0;
	int currentCounter = -1;
	int removeCount = -1;
	int lowHoursCount = 0;
	size_t lowHoursThreshold = noCostNightPatternsHighHours.size();
	while (running) {
		while (j < noCostNightPatterns.size()) {
			if (canReduceDemand(weeklyNightDemand, noCostNightPatterns[j])) {
				if ((j >= lowHoursThreshold && nightSetWithLowHourCount.first < 3) || j < lowHoursThreshold) {
					reduceDemand(weeklyNightDemand, noCostNightPatterns[j]);
					nightSetWithLowHourCount.second.push_back(noCostNightPatterns[j]);
					if (j >= lowHoursThreshold) {
						nightSetWithLowHourCount.first++;
					}
					if (accumulate(weeklyNightDemand.begin(), weeklyNightDemand.end(), 0) == 0) {
						nightSets.push_back(nightSetWithLowHourCount);
						removeLastWeekPattern(1, nightSetWithLowHourCount, weeklyNightDemand);
						if (j >= lowHoursThreshold) {
							nightSetWithLowHourCount.first--;
						}
					}
					else {
						addNewCounter(counters, j);
					}
				}
			}
			if (j == noCostNightPatterns.size() - 1) {
				if (nightSetWithLowHourCount.second.size() < 1) {
					std::cout << "EMPTY NIGHT SET?!?!" << std::endl;
					return;
				}
				removeCount = getAndIncrementLastCounter(counters, noCostNightPatterns.size(),
					nightSetWithLowHourCount.first, lowHoursThreshold);
				removeLastWeekPattern(removeCount, nightSetWithLowHourCount, weeklyNightDemand);
				currentCounter = getAndRemoveLastCounter(counters);
				if (currentCounter > noCostNightPatterns.size() - 1) {
					running = false;
					break;
				}
				j = currentCounter;
			}
			else {
				j++;
			}
		}
	}
}

void PatternManager::removeLastWeekPattern(int count, std::pair<int, std::vector<ShiftCombination>> &nightSet, std::array<int, WEEK_LENGTH> &demand) {
	for (int i = 0; i < count; i++) {
		if (nightSet.second.size() < 1) {
			std::cout << "WRONG NIGHTSET SIZE!!" << std::endl;
		}
		ShiftCombination removedSet = nightSet.second[nightSet.second.size() - 1];
		for (int i = 0; i < WEEK_LENGTH; i++) {
			if (removedSet[i] == ShiftCombination::ShiftType::NIGHT) {
				demand[i]++;
			}
		}
		nightSet.second.pop_back();
	}
}

void PatternManager::printNightSets(std::vector<std::pair<int, std::vector<ShiftCombination>>>& nightSets) {
	std::cout << nightSets.size() << " NightSets" << std::endl;
	int low0Count = 0;
	int low1Count = 0;
	int low2Count = 0;
	int low3Count = 0;
	for (std::pair<int, std::vector<ShiftCombination>> nightSet : nightSets) {
		if (nightSet.first == 0) {
			low0Count++;
		}
		if (nightSet.first == 1) {
			low1Count++;
		}
		if (nightSet.first == 2) {
			low2Count++;
		}
		if (nightSet.first == 3) {
			low3Count++;
		}
	}
}

int PatternManager::getAndIncrementLastCounter(std::array<int, WEEK_LENGTH> &counters, size_t maxCount,
	int &lowHoursCount, int lowHoursThreshold) {
	int removeCount = 1;
	for (int i = 0; i < counters.size(); i++) {
		if (counters[i] == -1) {
			if (counters[i - 1] >= lowHoursThreshold) {
				lowHoursCount--;
			}
			counters[i - 1]++;
			for (int j = i - 1; j > 0; j--) {
				if (counters[j] >= maxCount) {
					counters[j] = -1;
					counters[j - 1]++;
					if (counters[j - 1] >= lowHoursThreshold) {
						lowHoursCount--;
					}
					removeCount++;
				}
			}
			break;
		}
	}
	return removeCount;
}

int PatternManager::getAndRemoveLastCounter(std::array<int, WEEK_LENGTH> &counters) {
	int result;
	for (int i = 0; i < counters.size(); i++) {
		if (counters[i] == -1) {
			result = counters[i - 1];
			counters[i - 1] = -1;
			return result;
		}
	}
	result = counters[counters.size() - 1];
	counters[counters.size() - 1] = -1;
	return counters[counters.size() - 1];
}

void PatternManager::addNewCounter(std::array<int, WEEK_LENGTH> &counters, int count) {
	for (int i = 0; i < counters.size(); i++) {
		if (counters[i] == -1) {
			counters[i] = count;
			break;
		}
	}
}

void PatternManager::reduceDemand(std::array<int, WEEK_LENGTH> &demand, ShiftCombination &combination) {
	for (int i = 0; i < demand.size(); i++) {
		if (combination[i] == ShiftCombination::ShiftType::NIGHT) {
			demand[i]--;
		}
	}
}

bool PatternManager::canReduceDemand(std::array<int, WEEK_LENGTH> demand, ShiftCombination &combination) {
	for (int i = 0; i < demand.size(); i++) {
		if (demand[i] == 0 && combination[i] == ShiftCombination::ShiftType::NIGHT) {
			return false;
		}
	}
	return true;
}

void PatternManager::printPatterns(std::vector<ShiftCombination> &patterns) {
	std::shared_ptr<Nurse> lowHourNurse = std::make_shared<Nurse>(0, 20, true);
	std::shared_ptr<Nurse> midHourNurse = std::make_shared<Nurse>(1, 32, true);
	std::shared_ptr<Nurse> highHourNurse = std::make_shared<Nurse>(2, 36, true);
	int count = 0;
	std::string a;
	int cost = 0;
	for (int i = 0; i < patterns.size(); i++) {
		if (i < 12) {
			cost = ScheduleChecker::getCostOfPartialSchedule(highHourNurse, patterns[i]);
		}
		if (i == 12) {
			cost = ScheduleChecker::getCostOfPartialSchedule(midHourNurse, patterns[i]);
		}
		if (i > 12) {
			cost = ScheduleChecker::getCostOfPartialSchedule(lowHourNurse, patterns[i]);
		}
		std::cout << ++count << ": " << patterns[i].toString() << " Cost: " << cost << std::endl;
	}
}

int PatternManager::getNurseCost(ShiftCombination &pattern, int nurse) {
	std::shared_ptr<Nurse> lowHourNurse = std::make_shared<Nurse>(0, 20, true);
	std::shared_ptr<Nurse> midHourNurse = std::make_shared<Nurse>(1, 32, true);
	std::shared_ptr<Nurse> highHourNurse = std::make_shared<Nurse>(2, 36, true);
	int cost = 0;
	if (nurse < 12) {
		cost = ScheduleChecker::getCostOfPartialSchedule(highHourNurse, pattern)
		+ SoftPartialConstraints::calculateDivisionCost(pattern);
	}
	if (nurse == 12) {
		cost = ScheduleChecker::getCostOfPartialSchedule(midHourNurse, pattern)
			+ SoftPartialConstraints::calculateDivisionCost(pattern);
	}
	if (nurse > 12) {
		cost = ScheduleChecker::getCostOfPartialSchedule(lowHourNurse, pattern)
			+ SoftPartialConstraints::calculateDivisionCost(pattern);
	}

	return cost;
}

void PatternManager::printExtendedPatterns(std::vector<ShiftCombination> &patterns) {
	std::shared_ptr<Nurse> lowHourNurse = std::make_shared<Nurse>(0, 20, true);
	std::shared_ptr<Nurse> midHourNurse = std::make_shared<Nurse>(1, 32, true);
	std::shared_ptr<Nurse> highHourNurse = std::make_shared<Nurse>(2, 36, true);
	int count = 0;
	std::string a;
	int sumCost = 0;
	int cost = 0;
	for (int i = 0; i < patterns.size(); i++) {
		cost = getNurseCost(patterns[i], i);
		sumCost += cost;
		std::cout << ++count << ": " << patterns[i].toExtendedString() << " Cost: " << cost << std::endl;
	}
	std::cout << "Sum cost: " << sumCost << std::endl;
}