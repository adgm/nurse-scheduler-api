#pragma once
#include "Nurse.h"
#include "ShiftCombination.h"
#include <memory>

class IHardConstraint {
public:
	virtual ~IHardConstraint() {}
	virtual bool checkPatternValidity(ShiftCombination &p) = 0;
	virtual bool checkScheduleValidity(ShiftCombination &p, std::shared_ptr<Nurse> nurse) = 0;
};
