#pragma once
#include "ShiftCombination.h"

class Scheduler{
public:
	static void generateNextWeek(bool succesfulGeneration, int cutOff, bool lastGeneration);
	static int goBackToLastWeek();
	static std::vector<std::pair<int, ShiftCombination>> getNightSetIndexes(std::vector<ShiftCombination> &nightSet);
	static bool isNightInNightsVector(std::vector<std::pair<int, ShiftCombination>>& uniqueNights, ShiftCombination& night);
	static void updateDayDemand(std::array<int, WEEK_LENGTH> &demandLeft,
		std::vector<ShiftCombination> nightSet);
	static bool generateDaySet(int cutOff, std::array<int, WEEK_LENGTH> &demandLeft,
		std::vector<std::pair<int, ShiftCombination>> nights, std::vector<ShiftCombination> &daySet);
	static bool isIndexInNightVector(std::vector<std::pair<int, ShiftCombination>> nights, int index);
	static int getLowHoursLeftAmount(std::vector<std::pair<int, ShiftCombination>> nights);
	static bool meetsDemand(ShiftCombination &pattern, std::array<int, WEEK_LENGTH> &demandLeft);
	static bool meetsRecommendedDays(ShiftCombination &pattern, int recommendedDays);
	static void reduceDemand(ShiftCombination &pattern, std::array<int, WEEK_LENGTH> &demandLeft);
	static void returnDemand(ShiftCombination &pattern, std::array<int, WEEK_LENGTH> &demandLeft);
	static int getAmountOfDayShifts(ShiftCombination& pattern);
	static void resetTemporaries() { daySetFoundOnNights.clear(); }
private:
	static std::vector<int> daySetFoundOnNights;
};
