#pragma once
#include <vector>
#include <iostream>
#include <string>
#include "ISoftConstraint.h"
#include "IHardConstraint.h"
#include <algorithm>
#include "PatternSelector.h"
#include <set>
#include "ScheduleChecker.h"

#define NURSE_NUMBER 16
class PatternManager {
public:
	PatternManager(){
		noCostNightPatternsLowHours.reserve(20);
		noCostDayPatternsLowHours.reserve(20);
		noCostNightPatternsHighHours.reserve(20);
		noCostDayPatternsHighHours.reserve(20);
		lowCostNightPatternsLowHours.reserve(20);
		lowCostDayPatternsLowHours.reserve(20);
		lowCostNightPatternsHighHours.reserve(20);
		lowCostDayPatternsHighHours.reserve(20);
	}

	void generatePatterns();
	void printPatterns();
	void printNightSets(std::vector<std::pair<int, std::vector<ShiftCombination>>>& nightSets);
	void generateNightSets(bool withCost);
	static void printPatterns(std::vector<ShiftCombination> &patterns);
	static int getNurseCost(ShiftCombination& pattern, int nurse);
	static void printExtendedPatterns(std::vector<ShiftCombination>& patterns);

	int getCostOfPartialSchedule(std::shared_ptr<Nurse> nurse, ShiftCombination& combination) const{
		return ScheduleChecker::getCostOfPartialSchedule(nurse, combination);
	}

	void reduceDemand(std::array<int, WEEK_LENGTH> &demand, ShiftCombination &combination);
	bool canReduceDemand(std::array<int, WEEK_LENGTH> demand, ShiftCombination &combination);
	void addNewCounter(std::array<int, WEEK_LENGTH> &counters, int count);
	int getAndIncrementLastCounter(std::array<int, WEEK_LENGTH> &counters, size_t maxCount,
		int &lowHoursCount, int lowHoursThreshold);
	int getAndRemoveLastCounter(std::array<int, WEEK_LENGTH> &counters);
	void removeLastWeekPattern(int count, std::pair<int, std::vector<ShiftCombination>> &nightSet, std::array<int, WEEK_LENGTH> &demand);


	std::vector<std::array<ShiftCombination, NURSE_NUMBER>> weeklySchedules;
	std::vector<std::array<ShiftCombination, NURSE_NUMBER>> costlyWeeklySchedules;

	static std::vector<ShiftCombination> noCostNightPatternsLowHours;
	static std::vector<ShiftCombination> noCostDayPatternsLowHours;

	static std::vector<ShiftCombination> noCostNightPatternsHighHours;
	static std::vector<ShiftCombination> noCostDayPatternsHighHours;

	static std::vector<ShiftCombination> lowCostNightPatternsLowHours;
	static std::vector<ShiftCombination> lowCostDayPatternsLowHours;

	static std::vector<ShiftCombination> lowCostNightPatternsHighHours;
	static std::vector<ShiftCombination> lowCostDayPatternsHighHours;

	static std::vector<std::pair<int, std::vector<ShiftCombination>>> nightSets;
private:
	void iterateCombinationsWithDepth(int depth, ShiftCombination::ShiftType maxShiftValue);
	void addPatternIfEligible(ShiftCombination &pattern);
	void addPatternTo(std::shared_ptr<Nurse> nurse, ShiftCombination &pattern,
		std::vector<ShiftCombination> &dayVect, std::vector<ShiftCombination> &nightVect,
		int cost, bool satisfyHelper = true) const;
	static void incrementShiftOnDayInPattern(ShiftCombination& pattern, int index);
};
