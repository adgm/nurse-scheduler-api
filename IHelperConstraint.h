#pragma once
#include "Nurse.h"
#include "ShiftCombination.h"
#include <memory>

class IHelperConstraint {
public:
	virtual ~IHelperConstraint() {}
	virtual bool checkPatternValidity(ShiftCombination& pattern, std::shared_ptr<Nurse> nurse) = 0;
};
