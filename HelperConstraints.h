#pragma once
#include <vector>
#include "Nurse.h"
#include "IHelperConstraint.h"
class ShiftCombination;

class HelperConstraints : public IHelperConstraint {
public:
	bool checkPatternValidity(ShiftCombination& pattern, std::shared_ptr<Nurse> nurse) override;
	static bool hasNoIsolatedNightShiftOnEdge(ShiftCombination& shifts, std::shared_ptr<Nurse> nurse);
	static bool hasProperShiftNumberPerWeek(ShiftCombination& shifts, std::shared_ptr<Nurse> nurse);
	static bool hasLessThanGivenFreeWeekendsPerSchedule(ShiftCombination& shifts, int given);
	static bool hasFreeWeekInPattern(ShiftCombination& shifts);
	static bool isValidShiftNumber(ShiftCombination& shifts, std::shared_ptr<Nurse> nurse);
};