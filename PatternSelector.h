#pragma once
#include <array>
#include <memory>
#include "Nurse.h"
#include "IHelperConstraint.h"
#include "SoftPartialConstraints.h"
#include "HardPartialConstraints.h"
#include "ScheduleChecker.h"

#define WEEK_LENGTH 7
#define SCHEDULE_LENGTH 35
#define SCHEDULE_WEEKS 5

class PatternSelector{
public:

	enum PATTERN_TYPE{
		BALANCED_DEMANDED = 0,
		UNBALANCED_DEMANDED = 1,
		BALANCED_UNDEMANDED = 2,
		UNBALANCED_UNDEMANDED = 3
	};

	PatternSelector(){
		clearRequirements();
	}
	
	int selectSuggestedBalancedPatternWithDemand(std::shared_ptr<Nurse> nurse, std::vector<ShiftCombination>& vect, int week, int daySuggestion);
	int selectSuggestedBalancedPatternWithoutDemand(std::shared_ptr<Nurse> nurse, std::vector<ShiftCombination>& vect, int week, int daySuggestion);
	int selectSuggestedUnbalancedPatternWithDemand(std::shared_ptr<Nurse> nurse, std::vector<ShiftCombination>& vect, int week, int daySuggestion, bool fix);
	int selectSuggestedUnbalancedPatternWithoutDemand(std::shared_ptr<Nurse> nurse, std::vector<ShiftCombination>& vect, int week, int daySuggestion);
	int getAmountOfDayShiftsLeft(int week) const;


	void clearRequirements();
	void printDebug(int nurse, int shiftsUsed, int week);
	std::array<int, SCHEDULE_LENGTH> getNightsToAdd();
	std::array<int, SCHEDULE_LENGTH> getDaysToAdd();
	std::array<int, SCHEDULE_LENGTH> getNightsToErase();
	std::array<int, SCHEDULE_LENGTH> getDaysToErase();
	std::array<int, SCHEDULE_LENGTH> getDayRequirements() { return dayRequirements; }
	std::array<int, SCHEDULE_LENGTH> getNightRequirements(){ return nightRequirements; }
private:
	std::array<int, SCHEDULE_LENGTH> dayRequirements;
	std::array<int, SCHEDULE_LENGTH> nightRequirements;
	int selectBalancedPatternWithDemand(std::shared_ptr<Nurse> nurse, std::vector<ShiftCombination>& vect, int week, int daySuggestion);
	bool checkIfFreeWeekend(std::shared_ptr<Nurse> nurse, ShiftCombination& combination, int week) const;
	int selectBalancedPatternWithoutDemand(std::shared_ptr<Nurse> nurse, std::vector<ShiftCombination>& vect, int week, int daySuggestion);
	int selectUnbalancedPatternWithDemand(std::shared_ptr<Nurse> nurse, std::vector<ShiftCombination>& vect, int week, int daySuggestion, bool fix);
	int selectUnbalancedPatternWithoutDemand(std::shared_ptr<Nurse> nurse, std::vector<ShiftCombination>& vect, int week, int daySuggestion);
	int selectSuggestedBalancedPattern(std::shared_ptr<Nurse> nurse, ShiftCombination& combination, int week, int daySuggestion);
	int selectSuggestedUnbalancedPattern(std::shared_ptr<Nurse> nurse, ShiftCombination& combination, int week, int daySuggestion, bool fix = false);
	int selectBalancedPattern(std::shared_ptr<Nurse> nurse, ShiftCombination& combination, int week);
	int selectUnbalancedPattern(std::shared_ptr<Nurse> nurse, ShiftCombination& combination, int week, bool fix);
	void retractDayAndNightRequirements(ShiftCombination& pattern, int week);
	int selectPatternWithCost(std::shared_ptr<Nurse> nurse, ShiftCombination& combination, int week, int cost) const;

	static std::array<int, SCHEDULE_LENGTH> getShiftsToAdd(std::array<int, SCHEDULE_LENGTH> &requirements);
	static std::array<int, SCHEDULE_LENGTH> getShiftsToErase(std::array<int, SCHEDULE_LENGTH>& requirements);
	std::array<int, WEEK_LENGTH> getRequirementsForWeek(std::array<int, SCHEDULE_LENGTH> requirements, int week) const;
	bool isDemandBalanced(std::shared_ptr<Nurse> nurse, ShiftCombination &combination, int week);
	bool isMaxDifferenceSameOrLower(std::array<int, WEEK_LENGTH> &newDayRequirements, int week) const;
	bool isMaxChangedInCombination(ShiftCombination &combination, int week);
	bool hasNoNegativeImpactOnDemand(std::array<int, WEEK_LENGTH> &newDayRequirements,
		std::array<int, WEEK_LENGTH> &newNightRequirements, int week) const;
	static bool isNotCompletelyIsolated(std::array<int, WEEK_LENGTH> newDayRequirements);
	int getSumValue(std::array<int, WEEK_LENGTH> &requirements) const;
	static int getMaxValue(std::array<int, WEEK_LENGTH> &dayRequirements);
	static int getBiggestDayDifference(std::array<int, WEEK_LENGTH> &dayRequirements);
	static int getDayDifference(int from, int to, std::array<int, WEEK_LENGTH> &dayRequirements);
	static void updateCombination(std::shared_ptr<Nurse> nurse, ShiftCombination& pattern, int week);
	bool patternFulfillsRequirements(ShiftCombination &pattern, int week) const;
	static void adjustRequirements(std::array<int, SCHEDULE_LENGTH> &requirements,
		ShiftCombination& pattern, ShiftCombination::ShiftType type, int week);
	static void adjustRequirements(std::array<int, WEEK_LENGTH> &requirements, ShiftCombination &pattern, ShiftCombination::ShiftType type);

	void adjustDayAndNightRequirements(ShiftCombination& pattern, int week);
};
