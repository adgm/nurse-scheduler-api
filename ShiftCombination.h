#pragma once
#include <vector>
#include <iostream>
#include "CombinationsExceptions.h"
#define WEEK_LENGTH 7
class ShiftCombination {
public:
	enum ShiftType {
		REST,
		DAY,
		NIGHT,
		END
	};

	enum ExtendedShiftType {
		RESTING,
		EARLY,
		MID,
		LATE,
		NIGHTS,
	};

	ShiftCombination() : ShiftCombination(WEEK_LENGTH) {}

	explicit ShiftCombination(int combinationSize) {
		combinations.resize(combinationSize);
	}

	ShiftType operator[] (int x) const {
		return combinations[x];
	}

	ExtendedShiftType getExtendedShift(int x) const {
		return extendedCombinations[x];
	}

	size_t getExtendedCombinationSize() const {
		return extendedCombinations.size();
	}

	bool operator==(const ShiftCombination& rhs) const {
		if (getCombinationSize() != rhs.getCombinationSize()) {
			return false;
		}
		for (int i = 0; i < this->getCombinationSize(); i++) {
			if ((*this)[i] != rhs[i]) {
				return false;
			}
		}
		return true;
	}

	void resize(size_t size) {
		combinations.resize(size);
	}

	void changeShiftOnDay(int dayOfCombination, ShiftType shift) {
		if (isValidDay(dayOfCombination)) {
			combinations[dayOfCombination] = shift;
		}
		else {
			throw IncorrectDayException();
		}
	}

	void appendExtendedShiftOnDay(ExtendedShiftType shift) {
		extendedCombinations.push_back(shift);
	}

	void resizeExtended(size_t size){
		extendedCombinations.resize(size);
	}

	void changeExtendedShiftOnDay(int dayOfCombination, ExtendedShiftType shift) {
		if (isValidExtendedDay(dayOfCombination)) {
			extendedCombinations[dayOfCombination] = shift;
		}
		else {
			throw IncorrectDayException();
		}
	}

	int getNightShiftsCount() const {
		return getCombination(NIGHT);
	}

	int getDayShiftsCount() const {
		return getCombination(DAY);
	}

	int hasFreeWeekend() const {
		if (combinations[4] != NIGHT
			&& combinations[5] == REST
			&& combinations[6] == REST)
			return 1;
		return 0;
	}

	int getWorkingShiftsCount() const {
		return getNightShiftsCount() + getDayShiftsCount();
	}

	size_t getCombinationSize() const {
		return combinations.size();
	}

	std::string toString() {
		std::string patternString;
		for (int i = 0; i < getCombinationSize(); i++) {
			if (combinations[i] == DAY) {
				patternString += "D ";
			}
			if (combinations[i] == NIGHT) {
				patternString += "N ";
			}
			if (combinations[i] == REST) {
				patternString += "R ";
			}
			if ((i + 1) % 7 == 0) {
				patternString += " ";
			}
		}
		return patternString;
	}

	std::string toExtendedString() {
		std::string patternString;
		for (int i = 0; i < getExtendedCombinationSize(); i++) {
			if (extendedCombinations[i] == EARLY) {
				patternString += "E ";
			}
			if (extendedCombinations[i] == MID) {
				patternString += "D ";
			}
			if (extendedCombinations[i] == LATE) {
				patternString += "L ";
			}
			if (extendedCombinations[i] == RESTING) {
				patternString += "R ";
			}
			if (extendedCombinations[i] == NIGHTS) {
				patternString += "N ";
			}
			if ((i + 1) % 7 == 0) {
				patternString += " ";
			}
		}
		return patternString;
	}

	void appendCombination(ShiftCombination &shifts) {
		std::vector<ShiftType> shiftsToAppend = shifts.combinations;
		combinations.insert(combinations.end(), shiftsToAppend.begin(), shiftsToAppend.end());
	}

	void replaceLastWeekShifts(ShiftCombination &shifts) {
		std::vector<ShiftType> shiftsToAppend = shifts.combinations;
		if (shiftsToAppend.size() < WEEK_LENGTH || combinations.size() < WEEK_LENGTH) {
			throw IncorrectPatternException();
		}
		copy(shiftsToAppend.begin(), shiftsToAppend.begin() + WEEK_LENGTH, combinations.end() - WEEK_LENGTH);
	}

	void removeLastWeekShifts() {
		combinations.resize(combinations.size() - WEEK_LENGTH);
	}

	ShiftCombination getLast4Weeks() {
		ShiftCombination newCombination(28);
		std::copy(combinations.begin() + WEEK_LENGTH, combinations.end(), newCombination.combinations.begin());
		std::copy(extendedCombinations.begin() + WEEK_LENGTH,
			extendedCombinations.end(), newCombination.extendedCombinations.begin());
		return newCombination;
	}

	void copyLast4WeeksExtendedCombinations(ShiftCombination &destination){
		destination.extendedCombinations.resize(extendedCombinations.size() - WEEK_LENGTH);
		for(int i = WEEK_LENGTH; i < extendedCombinations.size(); i++){
			destination.extendedCombinations[i - WEEK_LENGTH] = extendedCombinations[i];
		}
	}

	void readFromString(std::string pattern) {
		extendedCombinations.resize(pattern.size());
		//The coding of E,D,L,N,R shifts is 1,2,3,4,5
		for (int i = 0; i < pattern.size(); i++) {
			switch (pattern[i]) {
			case '1':
				combinations[i] = DAY;
				extendedCombinations[i] = EARLY;
				break;
			case '2':
				combinations[i] = DAY;
				extendedCombinations[i] = MID;
				break;
			case '3':
				combinations[i] = DAY;
				extendedCombinations[i] = LATE;
				break;
			case '4':
				combinations[i] = NIGHT;
				extendedCombinations[i] = NIGHTS;
				break;
			case '5':
				combinations[i] = REST;
				extendedCombinations[i] = RESTING;
				break;
			}
		}
	}

private:
	std::vector<ShiftType> combinations;
	std::vector<ExtendedShiftType> extendedCombinations;

	bool isValidDay(int dayOfCombination) const {
		return dayOfCombination >= 0 && dayOfCombination < getCombinationSize();
	}

	bool isValidExtendedDay(int dayOfCombination) const {
		return dayOfCombination >= 0 && dayOfCombination < getExtendedCombinationSize();
	}

	int getCombination(ShiftType type) const {
		int days = 0;
		for (const ShiftType &shift : combinations) {
			if (shift == type) {
				days++;
			}
		}
		return days;
	}
};
