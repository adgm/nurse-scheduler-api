#pragma once
#include <string>
#include "ShiftCombination.h"
#include <Poco/JSON/Object.h>
#include <array>
class JsonParser{
public:
	std::string parseSchedule(std::vector<ShiftCombination> schedule){
		std::ostringstream output;
		Poco::JSON::Object::Ptr root = new Poco::JSON::Object();
		Poco::JSON::Array::Ptr rootArray = new Poco::JSON::Array();
		Poco::JSON::Object::Ptr cost = new Poco::JSON::Object();
		for (int i = 0; i < 5 * WEEK_LENGTH; i++) {
				Poco::JSON::Object::Ptr day = new Poco::JSON::Object();
				day->set("number", i+1);
				day->set("dayOfTheWeek", i % 7);
				day->set("day", getDayString(i % 7));
				Poco::JSON::Object::Ptr shifts = new Poco::JSON::Object();
				Poco::JSON::Object::Ptr earlyShifts = new Poco::JSON::Object();
				Poco::JSON::Object::Ptr dayShifts = new Poco::JSON::Object();
				Poco::JSON::Object::Ptr lateShifts = new Poco::JSON::Object();
				Poco::JSON::Object::Ptr nightShift = new Poco::JSON::Object();

				fillJsonObjectWithShifts(ShiftCombination::ExtendedShiftType::EARLY, i, schedule, earlyShifts);
				fillJsonObjectWithShifts(ShiftCombination::ExtendedShiftType::MID, i, schedule, dayShifts);
				fillJsonObjectWithShifts(ShiftCombination::ExtendedShiftType::LATE, i, schedule, lateShifts);
				fillJsonObjectWithShifts(ShiftCombination::ExtendedShiftType::NIGHTS, i, schedule, nightShift);

				shifts->set("0", earlyShifts);
				shifts->set("1", dayShifts);
				shifts->set("2", lateShifts);
				shifts->set("3", nightShift);

				day->set("shifts", shifts);

				for(int i = 0; i < NURSE_NUMBER; i++){
					cost->set(std::to_string(i), PatternManager::getNurseCost(schedule[i], i));
				}
				rootArray->set(i, day);
		}
		root->set("days", rootArray);
		root->set("costs", cost);

		root->stringify(output);

		return output.str();
	}
private:
	void fillJsonObjectWithShifts(ShiftCombination::ExtendedShiftType shiftType, int day, 
		std::vector<ShiftCombination> schedule, Poco::JSON::Object::Ptr json){
		int counter = 0;
		for (int j = 0; j < schedule.size(); j++) {
			if (schedule[j].getExtendedShift(day) == shiftType) {
				json->set(std::to_string(counter++), j);
			}
		}
	}
	std::string getDayString(int number){
		static std::array<std::string, WEEK_LENGTH> dayNames = 
		{ "monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday" };
		return dayNames[number];
	}
};


