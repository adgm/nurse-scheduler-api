#pragma once
#include "ScheduleChecker.h"

SoftPartialConstraints ScheduleChecker::softConstraint;
HardPartialConstraints ScheduleChecker::hardConstraint;
HelperConstraints ScheduleChecker::helperConstraint;