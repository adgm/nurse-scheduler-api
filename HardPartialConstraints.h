#pragma once
#include "IHardConstraint.h"
#include <vector>
#include "ShiftCombination.h"
#include "HelperConstraints.h"

class ShiftCombination;

class HardPartialConstraints : public IHardConstraint{
public:
	bool checkPatternValidity(ShiftCombination& pattern) override;
	bool checkScheduleValidity(ShiftCombination& pattern, std::shared_ptr<Nurse> nurse) override;
	static bool hasProperRestAfterConsecutiveNights(ShiftCombination& shifts);
	static bool hasProperRestAfterSingleNight(ShiftCombination& shifts);
	static bool hasMinimumRestAmountPerWeek(ShiftCombination& shifts);
	static bool hasMaximum3NightsPerWholeSchedule(ShiftCombination& shifts);
	static bool hasMaximum4HoursOvertime(ShiftCombination& shifts, std::shared_ptr<Nurse> nurse);
	static bool hasAtLeastNFreeWeekendsPerSchedule(ShiftCombination& shifts, int freeWeekends);
private:
	static bool checkBasicValidity(ShiftCombination& pattern);
};
