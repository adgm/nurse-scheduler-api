#pragma once
#include "Nurse.h"
#include <memory>
#include "ShiftCombination.h"

class ISoftConstraint {
public:
	virtual ~ISoftConstraint() {}
	virtual int calculatePatternCost(ShiftCombination &p, std::shared_ptr<Nurse> nurse) = 0;
	virtual int calculateScheduleCost(ShiftCombination &p, std::shared_ptr<Nurse> nurse) = 0;
};
