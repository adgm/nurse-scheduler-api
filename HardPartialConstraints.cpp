#include "HardPartialConstraints.h"
#include "CombinationsExceptions.h"

bool HardPartialConstraints::checkBasicValidity(ShiftCombination& pattern) {
	return hasProperRestAfterConsecutiveNights(pattern)
		&& hasProperRestAfterSingleNight(pattern)
		&& hasMaximum3NightsPerWholeSchedule(pattern);
}

bool HardPartialConstraints::checkPatternValidity(ShiftCombination& pattern) {
	return checkBasicValidity(pattern) && hasMinimumRestAmountPerWeek(pattern);
}

bool HardPartialConstraints::checkScheduleValidity(ShiftCombination& pattern, std::shared_ptr<Nurse> nurse) {
	bool hasRequiredFreeWeekends = true;
	if(pattern.getCombinationSize() == WEEK_LENGTH * 4){
		hasRequiredFreeWeekends = hasAtLeastNFreeWeekendsPerSchedule(pattern, 1);
	}
	if(pattern.getCombinationSize() == WEEK_LENGTH * 5){
		hasRequiredFreeWeekends = hasAtLeastNFreeWeekendsPerSchedule(pattern, 2);
	}
	return checkPatternValidity(pattern) 
			&& hasMaximum4HoursOvertime(pattern, nurse)
			&& hasRequiredFreeWeekends;
}

bool HardPartialConstraints::hasProperRestAfterConsecutiveNights(ShiftCombination& shifts) {
	const int days_to_rest = 2;
	int restedDays = 0;
	for (int i = 0; i < shifts.getCombinationSize() - 2; i++) {
		if (shifts[i] == ShiftCombination::ShiftType::NIGHT
			&& shifts[i + 1] == ShiftCombination::ShiftType::NIGHT) {
			for (int j = i + 2; j < shifts.getCombinationSize(); j++) {
				if (shifts[j] == ShiftCombination::ShiftType::REST) {
					restedDays++;
				}
				else{
					if(shifts[j] == ShiftCombination::ShiftType::NIGHT && restedDays == 0){
						continue;
					}
					if (restedDays < days_to_rest) {
						return false;
					}
				}
			}
		}
	}
	return true;
}

bool HardPartialConstraints::hasProperRestAfterSingleNight(ShiftCombination& shifts) {
	const int days_of_rest = 1;
	for (int i = 0; i < shifts.getCombinationSize() - days_of_rest; i++) {
		if (shifts[i] == ShiftCombination::ShiftType::NIGHT
			&& shifts[i + 1] == ShiftCombination::ShiftType::DAY) {
			return false;
		}
	}
	return true;
}

bool HardPartialConstraints::hasMinimumRestAmountPerWeek(ShiftCombination& shifts) {
	for (int i = 0; i < shifts.getCombinationSize() / WEEK_LENGTH; i++) {
		int restCount = 0;
		for (int j = 0; j < WEEK_LENGTH; j++) {
			if (shifts[i * WEEK_LENGTH + j] == ShiftCombination::ShiftType::REST) {
				restCount++;;
			}
		}
		if(restCount == 0){
			return false;
		}
	}
	return true;
}

bool HardPartialConstraints::hasMaximum3NightsPerWholeSchedule(ShiftCombination& shifts) {
	int countOfNights = 0;
	for (int i = 0; i < shifts.getCombinationSize(); i++) {
		if (shifts[i] == ShiftCombination::ShiftType::NIGHT) {
			countOfNights++;
		}
	}
	return countOfNights <= 3;
}

/*
 * In case of 32 hours shift - amount of 5-day shifts in schedule
 * has to be below 3 in order to satisfy hours requirement
 */
bool HardPartialConstraints::hasMaximum4HoursOvertime(ShiftCombination& shifts, std::shared_ptr<Nurse> nurse) {
	if (nurse->getHoursRange() == Nurse::MIDHOURS) {
		int dayShifts5 = 0;
		int dayShifts4 = 0;
		for (int week = 0; week < shifts.getCombinationSize() / WEEK_LENGTH; week++) {
			int shiftNumber = 0;
			for (int i = 0; i < 7; i++) {
				ShiftCombination::ShiftType currentShift = shifts[WEEK_LENGTH * week + i];
				if (currentShift == ShiftCombination::ShiftType::NIGHT
					|| currentShift == ShiftCombination::ShiftType::DAY) {
					shiftNumber++;
				}
			}
			if (shiftNumber == 5) { dayShifts5++; }
			else if (shiftNumber == 4) { dayShifts4++; }
			else return false;
		}
		if (dayShifts5 > 2) {
			return false;
		}
		return true;
	}
	return true;
}

bool HardPartialConstraints::hasAtLeastNFreeWeekendsPerSchedule(ShiftCombination& shifts, int freeWeekends) {
	int freeWeeksCount = 0;
	for (int i = 4; i < shifts.getCombinationSize(); i += 7) {
		if (shifts[i] != ShiftCombination::ShiftType::NIGHT
			&& shifts[i + 1] == ShiftCombination::ShiftType::REST
			&& shifts[i + 2] == ShiftCombination::ShiftType::REST) {
			freeWeeksCount++;
		}
	}
	return freeWeeksCount >= freeWeekends;
}
