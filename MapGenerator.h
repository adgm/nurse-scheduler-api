#pragma once
#include <vector>
#include "ShiftCombination.h"
#include "Nurse.h"
#define LOW_COST 20
class MapGenerator{
public:
	static bool generateCurrentMappingFromFile(std::string filename);
	static bool generateCurrentScheduleMapping();
	static void generateCurrentDivisionMapping(std::vector<ShiftCombination>& combinations, int currentDay);
	static void generateCurrentDivisionMappingForNurse(std::vector<ShiftCombination>& combinations, int currentDay, int nurse);
	static void generateExtendedSchedules();
	static void MapGenerator::updateNeededLateShifts(std::vector<ShiftCombination> &schedule, int day,
		std::array<int, 3> &weekDemand, std::array<int, 3> &weekendDemand,
		std::vector<ShiftCombination::ExtendedShiftType>& appropriateShifts);
	static void generateExtendedSchedule(std::vector<ShiftCombination> &schedule);
	static void updateCurrentExtendedSchedule(std::vector<ShiftCombination>& schedule, std::vector<ShiftCombination::ExtendedShiftType>& extendedShifts);
	static void printCurrentMapping();
	static void printCurrentSchedule();
	static void trimScheduleToLast4Weeks();
	static void updateCurrentSchedule(std::vector<ShiftCombination> daySet,
		std::vector<std::pair<int, ShiftCombination>> nights);
	static void addToScheduleMappingIfEligible(int nurseNumber, std::vector<ShiftCombination> &patterns,
		std::vector<ShiftCombination> &costPatterns, std::shared_ptr<Nurse> nurse, std::vector<std::vector<ShiftCombination>> &scheduleMapping);
	static void removeLastWeekFromSchedule();
	static void getLastScheduleCopy();
	static void resetTemporaries();
	static bool meetsDemand(std::array<int, 3> &demand, ShiftCombination::ExtendedShiftType shift);
	static void reduceDemand(std::array<int, 3> &demand, ShiftCombination::ExtendedShiftType shift);



	static std::vector<std::vector<ShiftCombination>> currentDayScheduleMapping;
	static std::vector<std::vector<ShiftCombination>> currentNightScheduleMapping;
	static std::vector<std::vector<std::vector<ShiftCombination::ExtendedShiftType>>> MapGenerator::currentDivisionMapping;
	static std::vector<std::vector<std::vector<ShiftCombination>>> wholeDayScheduleMapping;
	static std::vector<std::vector<std::vector<ShiftCombination>>> wholeNightScheduleMapping;
	static std::vector<ShiftCombination> MapGenerator::lastScheduleCopy;
	static std::vector<ShiftCombination> schedule;
private:
	static std::vector<ShiftCombination> readScheduleFromFile(std::string filename);
};
