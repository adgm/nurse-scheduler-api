#pragma once
#include "ISoftConstraint.h"
class SoftPartialConstraints : public ISoftConstraint{
public:
	int calculatePatternCost(ShiftCombination& pattern, std::shared_ptr<Nurse> nurse) override;
	int calculateScheduleCost(ShiftCombination &p, std::shared_ptr<Nurse> nurse) override;
	static int calculateDivisionCost(ShiftCombination& pattern, int currentDay = -1);

	static int getCostOfIsolatedShifts(ShiftCombination& shifts);
	static int getCostOfNightSeries(ShiftCombination& shifts, std::shared_ptr<Nurse> nurse);
	static int getCostOfShiftsNumberPerWeek(ShiftCombination& shifts, std::shared_ptr<Nurse> nurse);
	static int getCostOfShiftsSeries(ShiftCombination& shifts, std::shared_ptr<Nurse> nurse);
	static int getCostOfRestBeforeNights(ShiftCombination& shifts, std::shared_ptr<Nurse> nurse);
	static int getCostOfEarlyAfterDayShift(ShiftCombination& shifts, int currentDay = -1);
	static int getCostOfEarlyShiftSeries(ShiftCombination& shifts, int currentDay = -1);
};
