#pragma once
#include "PatternManager.h"
#include <Math.h>
#include <array>

class Algorithm {
public:
	explicit Algorithm();
	void printCurrentSchedule();
	void execute();

private:
	std::array<int, 7> weeklyDayShifts = { 0 };
	std::vector<std::shared_ptr<Nurse>> nurses;
	PatternManager patternManager;
	std::vector<ShiftCombination> noCostNightPatterns;
	std::vector<ShiftCombination> noCostDayPatterns;
};
