#include "Scheduler.h"
#include "PatternManager.h"
#include "MapGenerator.h"
#include <numeric>

std::vector<int> Scheduler::daySetFoundOnNights;

void Scheduler::generateNextWeek(bool succesfulGeneration, int cutOff, bool lastGeneration) {
	std::vector<std::pair<int, std::vector<ShiftCombination>>> &nights = PatternManager::nightSets;
	std::vector<std::pair<int, ShiftCombination>> uniqueNights;
	std::vector<ShiftCombination> daySet;

	int failures = 0;
	bool failover = false;
	bool found = false;
	while (!found) {
		for (int i = 0; i < nights.size(); i++) {
			if(!succesfulGeneration){
				succesfulGeneration = true;
				i = goBackToLastWeek();
				while (i >= nights.size()) {
					i = goBackToLastWeek();
				}
			}
			if (!lastGeneration && failures > 200 && daySetFoundOnNights.size() > 0 || failover) {
				failover = false;
				failures = 0;
				i = goBackToLastWeek();
				while(i >= nights.size()){
					i = goBackToLastWeek();
				}
			}
			std::array<int, WEEK_LENGTH> demandLeft = { 9,9,9,9,9,6,6 };
			daySet.clear();
			uniqueNights = getNightSetIndexes(nights[i].second);
			if (uniqueNights.size() == nights[i].second.size()) {
				updateDayDemand(demandLeft, nights[i].second);
				if (generateDaySet(cutOff, demandLeft, uniqueNights, daySet)
					&& std::accumulate(demandLeft.begin(), demandLeft.end(), 0) == 0) {
					found = true;
					daySetFoundOnNights.push_back(i);
					MapGenerator::updateCurrentSchedule(daySet, uniqueNights);
					break;
				}
//				std::cout << "Not Found... Nights: ";
//				for (auto night : uniqueNights) {
//					std::cout << night.first << ", ";
//				}
//				std::cout << std::endl;
				failures++;
			}
		}
		if (lastGeneration) {
			return;
		}
		failover = true;
	}
}

int Scheduler::goBackToLastWeek(){
	std::cout << "Go back to previous week..." << std::endl;
	int lastFoundNight = daySetFoundOnNights[daySetFoundOnNights.size() - 1] + 1;
	daySetFoundOnNights.pop_back();
	MapGenerator::removeLastWeekFromSchedule();
	return lastFoundNight;
}

std::vector<std::pair<int, ShiftCombination>> Scheduler::getNightSetIndexes(std::vector<ShiftCombination> &nightSet) {
	std::vector<std::vector<ShiftCombination>> &currentNightMapping = MapGenerator::currentNightScheduleMapping;
	std::vector<std::pair<int, ShiftCombination>> uniqueNights;
	for (int i = 0; i < currentNightMapping.size(); i++) {
		bool found = false;
		for (ShiftCombination &combination : currentNightMapping[i]) {
			for (ShiftCombination &night : nightSet) {
				if (combination == night
					&& !isNightInNightsVector(uniqueNights, night)) {
					uniqueNights.push_back(std::make_pair(i, night));
					found = true;
				}
				if (uniqueNights.size() == nightSet.size()) {
					return uniqueNights;
				}
				if (found) {
					break;
				}
			}
			if (found) {
				break;
			}
		}
	}
	return uniqueNights;
}

bool Scheduler::isNightInNightsVector(std::vector<std::pair<int, ShiftCombination>> &uniqueNights, ShiftCombination &night){
	if(uniqueNights.size() == 0){
		return false;
	}
	for(int i = 0; i < uniqueNights.size(); i++){
		if(uniqueNights[i].second == night){
			return true;
		}
	}
	return false;
}

void Scheduler::updateDayDemand(std::array<int, WEEK_LENGTH> &demandLeft,
	std::vector<ShiftCombination> nightSet) {
	for (ShiftCombination &night : nightSet) {
		for (int i = 0; i < demandLeft.size(); i++) {
			if (night[i] == ShiftCombination::ShiftType::DAY) {
				demandLeft[i]--;
			}
		}
	}
}

bool Scheduler::generateDaySet(int cutOff, std::array<int, WEEK_LENGTH> &demandLeft,
	std::vector<std::pair<int, ShiftCombination>> nights, std::vector<ShiftCombination> &daySet) {
	int cutOffCounter = 0;
	std::vector<std::vector<ShiftCombination>> &currentDayMapping = MapGenerator::currentDayScheduleMapping;
	bool running = true;
	int depth = 0;
	std::vector<int> depthIndexes;
	int i = 0;
	size_t nursesLeft = NURSE_NUMBER - nights.size();
	int lowHourNurses = getLowHoursLeftAmount(nights);
	size_t highHourNurses = nursesLeft - lowHourNurses;
	int recommendedDays;
	while (true) {
		while (depth < 16 && i < currentDayMapping[depth].size()) {
			if(cutOffCounter > cutOff){
				return false;
			}
			cutOffCounter++;

			while (isIndexInNightVector(nights, depth)) {
				depth++;
				i = 0;
			}

			if(depth > 15){
				return true;
			}
			int demandSum = std::accumulate(demandLeft.begin(), demandLeft.end(), 0);
			if (depth >= 13) {
				if (demandSum / nursesLeft > 2.8) {
					recommendedDays = 3;
				}
				else {
					recommendedDays = 0;
				}
			}
			else {
				if (demandSum / (nursesLeft - lowHourNurses) > 4.8) {
					recommendedDays = 5;
				}
				else if(demandSum / (nursesLeft - lowHourNurses) < 4.2){
					recommendedDays = 4;
				}
				else {
					recommendedDays = 0;
				}
			}

			if (meetsDemand(currentDayMapping[depth][i], demandLeft)
				) {
				reduceDemand(currentDayMapping[depth][i], demandLeft);
				daySet.push_back(currentDayMapping[depth][i]);
				depthIndexes.push_back(i);
				depth++;
				i = 0;
				nursesLeft--;
			}
			else {
				i++;
			}
		}

		if(depth > 15){
			return true;
		}

		depth--;

		while (isIndexInNightVector(nights, depth)) {
			depth--;
		}

		if(depth < 1){
			std::cout << ".";
		}

		if (depthIndexes.size() == 0 && depth == -1) {
//			std::cout << "No weeks found for night: ";
//			for(auto night : nights){
//				std::cout << night.first << ", ";
//			}
//			std::cout << std::endl;
			return false;
		}

		nursesLeft++;
		i = depthIndexes[depthIndexes.size() - 1] + 1;
		ShiftCombination removedCombination = daySet[daySet.size() - 1];
		depthIndexes.pop_back();
		daySet.pop_back();
		returnDemand(removedCombination, demandLeft);
	}
}

bool Scheduler::isIndexInNightVector(std::vector<std::pair<int, ShiftCombination>> nights, int index){
	for(auto nightPair : nights){
		if(nightPair.first == index){
			return true;
		}
	}
	return false;
}

int Scheduler::getLowHoursLeftAmount(std::vector<std::pair<int, ShiftCombination>> nights) {
	int lowHoursLeft = 3;
	for (int i = 0; i < nights.size(); i++) {
		if (nights[i].first > 12) {
			lowHoursLeft--;
		}
	}
	return lowHoursLeft;
}


bool Scheduler::meetsDemand(ShiftCombination &pattern, std::array<int, WEEK_LENGTH> &demandLeft) {
	for (int i = 0; i < demandLeft.size(); i++) {
		if (pattern[i] == ShiftCombination::DAY && demandLeft[i] < 1) {
			return false;
		}
	}
	return true;
}

bool Scheduler::meetsRecommendedDays(ShiftCombination &pattern, int recommendedDays) {
	if(recommendedDays == 0){
		return true;
	}
	int dayCount = 0;
	for (int i = 0; i < pattern.getCombinationSize(); i++) {
		if (pattern[i] == ShiftCombination::DAY) {
			dayCount++;
		}
	}
	return dayCount == recommendedDays;
}

void Scheduler::reduceDemand(ShiftCombination &pattern, std::array<int, WEEK_LENGTH> &demandLeft) {
	for (int i = 0; i < demandLeft.size(); i++) {
		if (pattern[i] == ShiftCombination::DAY) {
			demandLeft[i]--;
		}
	}
}

void Scheduler::returnDemand(ShiftCombination &pattern, std::array<int, WEEK_LENGTH> &demandLeft) {
	for (int i = 0; i < demandLeft.size(); i++) {
		if (pattern[i] == ShiftCombination::DAY) {
			demandLeft[i]++;
		}
	}
}

int Scheduler::getAmountOfDayShifts(ShiftCombination &pattern) {
	int dayCount = 0;
	for (int i = 0; i < pattern.getCombinationSize(); i++) {
		if (pattern[i] == ShiftCombination::DAY) {
			dayCount++;
		}
	}
	return dayCount;
}